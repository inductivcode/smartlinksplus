package com.inductivtechnologies.smartlinksplusEjb.morphia;

import com.inductivtechnologies.smartlinksplusEjb.entities.account.User;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import java.util.logging.Logger;

/**
 * MongoDB providing the database connection for main.
 * Implementation et Construction d'une datasource mongodb
 */
public final class MongoDB {

	  /**
	   * constante DB_HOST, adresse du serveur mongodb 
	   */
	  public static final String DB_HOST = "127.0.0.1";
	  
	  /**
	   * constante DB_PORT, port du serveur mongodb 
	   */
	  public static final int DB_PORT = 27017;
//	  public static final int DB_PORT = 27019;
	  
	  /**
	   * constante DB_NAME, nom de la base de donnée mongodb 
	   */
	  public static final String DB_NAME = "smartlinksplusDB";
	  
	  /**
	   * constante LOG, utilisé pour l'émission et le stockage de messages suite aux traitements
	   *  
	   * @see Logger
	   */
	  private static final Logger LOG = Logger.getLogger(MongoDB.class.getName());
	  
	  /**
	   * constante INSTANCE, variable de classe MongoDB, unique instance de cette classe
	   *  
	   */
	  private static final MongoDB INSTANCE = new MongoDB();
	  
	  /**
	   * attribut datastore, mapping objet document, cet objet permettra de créer une relation entre les classes entity java et les 
	   * documents mongodb correspondant. Initialisé dans le constructeur et non modifiable, accès uniquement en lecture
	   * 
	   * @see Datastore
	   * @see MongoDB#getDatabase()
	   */
	  private final Datastore datastore;
	  
	  /**
	   * attribut db, représentation physique de la base de données, initialisé l'or dans le constructeur
	   *  
	   * et non modifiable accès uniquement en lecture
	   * 
	   * @see MongoDB#getDb()
	   */
	  private final DB db;
	  
	  /**
	   *   Constructeur de la classe initialise les Objets datastore et db
	   */
	  private MongoDB() {
	    MongoClientOptions mongoOptions = MongoClientOptions.builder()
		.socketTimeout(60000) // Wait 1m for a query to finish, https://jira.mongodb.org/browse/JAVA-1076
		.connectTimeout(15000) // Try the initial connection for 15s, http://blog.mongolab.com/2013/10/do-you-want-a-timeout/
		.maxConnectionIdleTime(600000) // Keep idle connections for 10m, so we discard failed connections quickly
		.readPreference(ReadPreference.primaryPreferred()) // Read from the primary, if not available use a secondary
		.build();
	    MongoClient mongoClient;
	    mongoClient = new MongoClient(new ServerAddress(DB_HOST, DB_PORT), mongoOptions);
	    mongoClient.setWriteConcern(WriteConcern.ACKNOWLEDGED);
	    datastore = new Morphia().mapPackage(User.class.getPackage().getName())
		.createDatastore(mongoClient, DB_NAME);
	    datastore.ensureIndexes();
	    datastore.ensureCaps();
	    db=new DB(mongoClient, DB_NAME);
	    LOG.info("Connection to database '" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "' initialized");
	  }
	  
	  /**
	   * instance, retourne l'unique instance de la classe
	   * 
	   * @return INSTANCE
	   */
	  public static MongoDB instance() {
	    return INSTANCE;
	  }
	  
	  /**
	   * getDatabase, methode d'accès au champ datastore, retourne la variable de classe 
	   * datastore
	   * @return datastore
	   */
	  // Creating the mongo connection is expensive - (re)use a singleton for performance reasons.
	  // Both the underlying Java driver and Datastore are thread safe.
	  public Datastore getDatabase() {
	    return datastore;
	  }
	
	  /**
	   * getDb, methode d'accès au champ db, retourne la variable de classe db
	   * 
	   * @return db
	   */
	  public DB getDb() {
		return db;
	  }
  
  
}