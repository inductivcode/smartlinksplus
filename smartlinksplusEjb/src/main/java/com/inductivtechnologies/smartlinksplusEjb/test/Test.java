package com.inductivtechnologies.smartlinksplusEjb.test;

import java.io.IOException;
import java.util.Date;

import org.joda.time.DateTime;

import com.inductivtechnologies.smartlinksplusEjb.ejb.account.impl.TokenDao;
import com.inductivtechnologies.smartlinksplusEjb.ejb.account.impl.UserDao;
import com.inductivtechnologies.smartlinksplusEjb.ejb.company.impl.CompanyDao;
import com.inductivtechnologies.smartlinksplusEjb.ejb.market.impl.RawMarketDataDao;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Credential;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.User;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.api.YahooFinanceApi;
import com.inductivtechnologies.smartlinksplusEjb.utils.CryptoUtils;
import com.inductivtechnologies.smartlinksplusEjb.utils.DateUtils;

/**
 * Main test
 */
public class Test {
	
	private static final String USERNAME = "InductivTech";
	private static final String PASSWORD = "AdminInductivTech2018++SmarLPL";
	
	
	public static void addUser(){
		//On génère un token 
		Token token = new Token();
		new TokenDao().add(token);
		
		//On crée un utilisateur 
		Credential credential = new Credential(USERNAME, CryptoUtils.hashPassword(PASSWORD));
		User user = new User();
		user.setCredentials(credential);
		user.setToken(token);
		
		new UserDao().add(user);
	}
	
	 public static void main(String[] args) throws NullPointerException, IOException{
//		 System.out.println(DateTime.now().getMillis() / 1000);
		 
//		 new RawMarketDataDao().dailyUpdateEquities();
		 
		 //Pour ajjouter un utilisateur 
//		 addUser();
		 
		 //Pour ajouter les compagnies 
//		 new CompanyDao().addAllCompanies();
		 
		 //Pour récuperer les symboles des fx spot depuis un fichier et les enregistrés
//		 new FxSpotSymbolDao().insertFxSpotSymbolFromCSVFile(Constants.CSV_FILE_PATH_FXSPOT_SYMBOLS);
		 
		 //Pour le téléchargement des equities depuis 2005  
//		 new RawMarketDataDao().allOldEquities();
		 
		 //Pour le téléchargement depuis des fx Spots 2005 
//		 new RawMarketDataDao().allOldFxSpot();
		
		 //Pour voir la liste des symboles des fx spot conenu dans la base de données
//		 List<FxSpotSymbol> datas = new FxSpotSymbolDao().allFxSpotSymbol();
//		 Util.viewFxSpotSymbols(datas);
		 
		 //Pour voir quelques equities conenu dans la base de données
//		 Date endDate = DateUtils.parseJavaDate("10-04-2018", Constants.PRINT_DATE_FORMAT);
//		 Date startDate = DateUtils.parseJavaDate("01-01-2018", Constants.PRINT_DATE_FORMAT);
		 
//		 //Pour voir quelques fx spot conenu dans la base de données
//		 List<RawMarketData> fxspots = new RawMarketDataDao()
//					.rawMarketDataBetweenPeriod("GBPUSD", Constants.FXSPOT_TYPE, startDate, endDate, 0, 0);
//		 System.out.println("Pour les dates " + 
//					DateUtils.printJavaDate(startDate, Constants.PRINT_DATE_FORMAT) + " et " + 
//					DateUtils.printJavaDate(endDate, Constants.PRINT_DATE_FORMAT));
//		 System.out.println(fxspots.size() + " RESULTATS ");
//		 Util.viewMarkets(fxspots);
//		 
		//Pour voir quelques equities conenu dans la base de données
//		 List<RawMarketData> equities = new RawMarketDataDao()
//					.rawMarketDataBetweenPeriod("SNR", Constants.EQUITY_TYPE, startDate, endDate, 0, 0);
//		 System.out.println("Pour les dates " + 
//					DateUtils.printJavaDate(startDate, Constants.PRINT_DATE_FORMAT) + " et " + 
//					DateUtils.printJavaDate(endDate, Constants.PRINT_DATE_FORMAT));
//		 System.out.println(equities.size() + " RESULTATS ");
//		 Util.viewMarkets(equities);
	 }
}
