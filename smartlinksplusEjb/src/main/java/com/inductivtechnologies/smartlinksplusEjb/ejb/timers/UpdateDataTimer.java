package com.inductivtechnologies.smartlinksplusEjb.ejb.timers;

import java.util.Date;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;

import com.inductivtechnologies.smartlinksplusEjb.ejb.company.inter.CompanyLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.RawMarketDataLI;
import com.inductivtechnologies.smartlinksplusEjb.utils.Constants;
import com.inductivtechnologies.smartlinksplusEjb.utils.DateUtils;

/**
 * Timer Session Bean TBCloseMarketOfferTest : Permet la mise à jour quotidienne des equities
 */
@Stateless
public class UpdateDataTimer {
	
	public static final String TAG = UpdateDataTimer.class.getSimpleName();
	
	/**
	 * rawMarketDAO Accès aux données (Marchés)
	 */
	@EJB
	private RawMarketDataLI rawMarketDAO;
	
	/**
	 * baseDao Accès aux données (Compagnies)
	 */
	@EJB
	private CompanyLI baseDao;
    
	/**
	 * Pour loger
	*/
	private static final Logger LOG = Logger.getLogger(TAG);
	  
    /**
     * dailyUpdate : Mise à jour quotidienne des données
     * @param timer
     */
    @Schedule(second ="00", minute="00", hour="09", dayOfWeek="*", dayOfMonth="*", month="*", year="*", 
    		info="dailyUpdate", persistent = false)
    private void dailyUpdate(final Timer timer) throws Exception {
    	
    	LOG.info("Mise à jour de la base de données ::: " + 
    				DateUtils.printJavaDate(new Date(),
    						Constants.PRINT_DATE_FORMAT)); 
    	
    	System.out.println("Début de mise a jour des compagnies");
    	//MAJ des compagnies
		baseDao.addAllCompanies();
		System.out.println("Fin de mise a jour des compagnies");
		
      	//MAJ des fx spot 
        rawMarketDAO.dailyUpdateFxSpot();
    	//MAJ des equities 
        rawMarketDAO.dailyUpdateEquities();
        
        LOG.info("Mise à jour de la base de données terminée ::: " + 
				DateUtils.printJavaDate(new Date(),
						Constants.PRINT_DATE_FORMAT)); 
    }
	
}
