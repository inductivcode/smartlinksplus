package com.inductivtechnologies.smartlinksplusEjb.ejb.market.impl;

import java.util.List;

import javax.ejb.Stateless;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.FxSpotSymbolLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.FxSpotSymbolRI;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusEjb.morphia.MongoDB;
import com.inductivtechnologies.smartlinksplusEjb.utils.Constants;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * FxSpotSymbolDao : Implémentation de l'interface FxSpotSymbolLI et FxSpotSymbolRI
 **/
@Stateless(mappedName = "fxSpotSymbolDAO")
public class FxSpotSymbolDao implements FxSpotSymbolLI, FxSpotSymbolRI {
	
	/**
	 * MongoDb utilisé pour l'accès au sgbd mongodb
	 */
	private final Datastore mongoDatastore;
	
	/**
	 * Constructeur 
	 */
	public FxSpotSymbolDao() {
		this.mongoDatastore = MongoDB.instance().getDatabase();
	}
	
	//Getter(s)
	public Datastore getMongoDatastore() {
		return mongoDatastore;
	}

	@Override
	public void insertFxSpotSymbol(FxSpotSymbol fxspotSymbol) {
		FxSpotSymbol fxSpotSymbol = findFxSpotSymbol(fxspotSymbol.getSymbolValue());
		
		if(fxSpotSymbol != null){
			fxspotSymbol.setFxSpotSymbolId(fxSpotSymbol.getFxSpotSymbolId());
			mongoDatastore.merge(fxspotSymbol);
		}else{
			mongoDatastore.save(fxspotSymbol);
		}
	}

	@Override
	public FxSpotSymbol findFxSpotSymbol(String symbolValue) {
		Query<FxSpotSymbol> query = mongoDatastore.find(FxSpotSymbol.class)
				.field("symbolValue").equal(symbolValue);
		
		if(query.asList().size() == 1) return query.asList().get(0);
		return null;
	}
	
	@Override
	public void insertListOfFxSpotSymbol(List<FxSpotSymbol> fxSpotSymbols){
		for(FxSpotSymbol fxSpotSymbol : fxSpotSymbols){
		   insertFxSpotSymbol(fxSpotSymbol);
//			String url = String
//					.format("https://www.investing.com/currencies/%s-%s-historical-data", 
//							fxSpotSymbol.getSymbolValue().substring(0, 3).toLowerCase(), 
//							fxSpotSymbol.getSymbolValue().substring(3).toLowerCase());
//			System.out.println(url);
		}
	}

	@Override
	public void insertFxSpotSymbolFromCSVFile(String csvFileName){
		System.out.println("**********************************************");
		System.out.println("Récupération des Fx spot symbols depuis le fichier " + 
							Constants.CSV_FILE_PATH_FXSPOT_SYMBOLS);
		
		List<FxSpotSymbol> fxSpotSymbols = Util.allFxSpotSymbolsFromCSVFile(csvFileName);
		if(fxSpotSymbols.isEmpty()){
			System.out.println("Récupération terminées aucun symbole trouvé");
		}else{
			System.out.println("Enrégistrement ...");
			//On sauvegarde 
			insertListOfFxSpotSymbol(fxSpotSymbols);
			System.out.println("Enrégistrement terminée ");
		}
		System.out.println("**********************************************");
	}

	@Override
	public List<FxSpotSymbol> allFxSpotSymbol() {
		return mongoDatastore.find(FxSpotSymbol.class).asList();
	}

}
