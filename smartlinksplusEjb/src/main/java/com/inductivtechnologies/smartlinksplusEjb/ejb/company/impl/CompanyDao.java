package com.inductivtechnologies.smartlinksplusEjb.ejb.company.impl;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;

import org.mongodb.morphia.query.Query;

import com.inductivtechnologies.smartlinksplusEjb.ejb.base.impl.BaseDao;
import com.inductivtechnologies.smartlinksplusEjb.ejb.company.inter.CompanyLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.company.inter.CompanyRI;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.Company;
import com.inductivtechnologies.smartlinksplusEjb.utils.HtmlScrapper;

/**
 * CompanyDao : implementation des interfaces CompanyLI et CompanyRI
 **/
@Stateless(mappedName = "companyDao")
public class CompanyDao extends BaseDao implements CompanyLI, CompanyRI {

	/**
	 * Constructeur 
	 */
	public CompanyDao(){
		super();
	}

	@Override
	public void addAllCompanies() throws NullPointerException, IOException{
		List<Company> lscomp = HtmlScrapper.loadCompagniesInfo();
		  
		for(Company comp : lscomp){
			Company oldComp = findCompanyBySymbol(comp.getSymbol());
			if(oldComp != null){
				comp.setIdCompany(oldComp.getIdCompany());
				update(comp);
			}else{
				add(comp);
			}
		}
	}

	@Override
	public Company findCompanyBySymbol(String symbol) {
		Query<Company> query = mongoDatastore.find(Company.class)
				.field("symbol").equal(symbol);
		
		if(query.asList().size() == 1) return query.asList().get(0);
 		return null;
	}

	@Override
	public List<Company> findAllCompanies() {
		return findAll(Company.class);
	}
}
