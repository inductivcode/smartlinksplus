package com.inductivtechnologies.smartlinksplusEjb.ejb.account.impl;

import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.TokenRI;

import javax.ejb.Stateless;

import org.joda.time.DateTime;
import org.mongodb.morphia.query.Query;

import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.TokenLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.base.impl.BaseDao;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;
import com.inductivtechnologies.smartlinksplusEjb.utils.DateUtils;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * TokenDao : Implémentation de l'interface TokenRI et TokenLI
*/
@Stateless(mappedName = "tokenDao")
public class TokenDao extends BaseDao implements TokenRI, TokenLI {

	/**
	 * Constructeur 
	 */
	public TokenDao() {
		super();
	}

	@Override
	public Token regenerateToken(Token token){
		//On prolonge la date d'expiration  
		DateTime now = DateTime.now();
		token.setExpirationDate(DateUtils.convertDateTimeToDate(now.plusHours(12)));
		
		//On modifie le token 
		token.setValue(Util.generateSecureToken(Token.TOKEN_LENGHT));
		
		//Mise à jour 
		update(token);
		
		return token;
	}

	@Override
	public Token findTokenByValue(String value) {
		Query<Token> query = mongoDatastore.find(Token.class)
				.field("value").equal(value);
		
		if(query.asList().size() == 1) return query.asList().get(0);
		return null;
	}
	
}
