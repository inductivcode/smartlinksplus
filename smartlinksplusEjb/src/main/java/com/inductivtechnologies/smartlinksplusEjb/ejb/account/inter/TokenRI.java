package com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter;

import javax.ejb.Remote;

import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;

/**
 * TokenRI : Interface distante, collection de services 
 * pour l'accès aux données de type Token
 */
@Remote
public interface TokenRI {

	/**
	 * Service regenerateToken : Permet de regénérer un token 
	 * @param token Le token à regénérer
	 * @return Le Token regénéré
	 */
	public Token regenerateToken(Token token);
	
	/**
	 * Service findTokenByValue : Vérifie si un token existe 
	 * @param value La valeur du token 
	 * @return Le token correspondant
	 */
	public Token findTokenByValue(String value);
	
}
