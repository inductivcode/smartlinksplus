package com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter;

import javax.ejb.Local;

import com.inductivtechnologies.smartlinksplusEjb.entities.account.Credential;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.User;

/**
 * UserRI : Interface distante, collection de services 
 * pour l'accès aux données de type User
 */
@Local
public interface UserRI {

	/**
	 * Service findUserByCredentials : Recherche un utilisateur à partir de ses informations
	 * d'authentification 
	 * @param credential Informations d'authentification 
	 * @return L'utilisateur correspondant ou null si aucune correspondance 
	 */
	public User findUserByCredentials(Credential credential);
	
	/**
	 * Service findUserByUsername : Recherche un utilisateur à partir de son nom utilisateur 
	 * @param username Nom utilisateur 
	 * @return L'utilisateur correspondant ou null si aucune correspondance 
	 */
	public User findUserByUsername(String username);
	
}
