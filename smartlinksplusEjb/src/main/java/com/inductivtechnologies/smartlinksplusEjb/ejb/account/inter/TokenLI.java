package com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter;

import javax.ejb.Local;

import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;

/**
 * TokenLI : Interface locale, collection de services 
 * pour l'accès aux données de type Token
 */
@Local
public interface TokenLI {

	/**
	 * Service regenerateToken : Permet de regénérer un token 
	 * @param token Le token à regénérer
	 * @return Le Token regénéré
	 */
	public Token regenerateToken(Token token);
	
	/**
	 * Service findTokenByValue : Vérifie si un token existe 
	 * @param value La valeur du token 
	 * @return Le token correspondant
	 */
	public Token findTokenByValue(String value);
	
}
