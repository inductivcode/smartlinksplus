package com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter;

import java.util.List;

import javax.ejb.Local;

import com.inductivtechnologies.smartlinksplusEjb.entities.market.FxSpotSymbol;

/**
 * FxSpotSymbolLI : Interface locale, collection de services 
 * pour l'accès aux données de type FxSpotSymbol
 */
@Local
public interface FxSpotSymbolLI {

	/**
	 * Service insertFxSpotSymbol : Ajoute un nouveau FxSpotSymbol
	 * @param fxspotSymbol Le symbole à insérer
	 */
	public void insertFxSpotSymbol(FxSpotSymbol fxspotSymbol);
	
	/**
	 * Service findFxSpotSymbol : Recherche un symbol en se basant sur sa valeur 
	 * @param symbolValue La valeur du cymbol
	 * @return FxSpotSymbol correspondant
	 */
	public FxSpotSymbol findFxSpotSymbol(String symbolValue);
	
	/**
	 * Service insertListOfFxSpotSymbol : Insère une liste de symboles
	 * @param fxSpotSymbols La liste à ajouter
	 */
	public void insertListOfFxSpotSymbol(List<FxSpotSymbol> fxSpotSymbols);
	
	/**
	 * Service insertFxSpotSymbolFromCSVFile : Insère des symboles provenant d'un fichier csv
	 * @param csvFileName Le nom du fichier csv contenat les symboles des fx spot 
	 */
	public void insertFxSpotSymbolFromCSVFile(String csvFileName);
	
	/**
	 * Service allFxSpotSymbol : Retourne tous les symboles fx spot
	 * @return Liste des symboles fx spot
	 */
	public List<FxSpotSymbol>  allFxSpotSymbol();
	
}
