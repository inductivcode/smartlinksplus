package com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import org.bson.types.ObjectId;

import com.inductivtechnologies.smartlinksplusEjb.entities.market.RawMarketData;

/**
 * RawMarketDataRI : Interface distante, collection de services 
 * pour l'accès aux données du RawMarketData
 */
@Remote
public interface RawMarketDataRI{
	
	/**
	 * Service insertRawMarketData : Ajoute un nouveau RawMarketData
	 * @param marketData Le marché à sauvegarder
	 */
	public void insertRawMarketData(RawMarketData marketData);
	
	/**
	 * Service updateRawMarketData : Met à jour RawMarketData
	 * @param marketData Le marché à sauvegarder
	 */
	public void updateRawMarketData(RawMarketData marketData);
	
	/**
	 * Service rawMarketDataBetweenPeriod : Récupère une liste de marchés entre deux dates 
	 * @param symbol Le symbole correspoondant
	 * @param type Le type du marché
	 * @param startDate Date de début
	 * @param endDate Date de fin
	 * @param skip Nombre de données à sauter
	 * @param limit Nombre de données à récuperer
	 * @return List<RawMarketData>  une liste de marché
	 */
	public List<RawMarketData> rawMarketDataBetweenPeriod(String symbol, String type, 
			Date startDate, Date endDate, int skip, int limit);
	
	/**
	 * Service rawMarketDataBySymbolAndType : Récupère une liste de marchés appartenant à un symbole
	 * @param symbol Le symbole correspoondant
	 *  * @param skip Nombre de données à sauter
	 * @param limit Nombre de données à récuperer
	 * @return List<RawMarketData>  une liste de marché
	 */
	public List<RawMarketData> rawMarketDataBySymbolAndType(String symbol, String type, int skip, int limit);
	
	/**
     * allOldEquities : Récupère et enregistre des equities depuis 2005
     */
	public void allOldEquities();
	
	/**
     * dailyUpdateEquities Mise à jour quotidienne des equities
     */
	public void dailyUpdateEquities();
	
	/**
     * allOldFxSpot : Récupère et enregistre des fx spot depuis 2005
     */
	public void allOldFxSpot();
	
	/**
     * dailyUpdateFxSpot Mise à jour quotidienne des fx spot
     */
	public void dailyUpdateFxSpot();
	
	/**
	 * insertListOfRawMarketData : Enregistre une liste de marchés
	 * @param markets La liste des marchés
	 */
	public void insertListOfRawMarketData(List<RawMarketData> markets);
	
	/**
	 * updateListOfRawMarketData : Met à jour une liste de marchés
	 * @param markets La liste des marchés
	 */
	public void updateListOfRawMarketData(List<RawMarketData> markets);
	
	/**
	 * findRawMarketData : Recherche un marché ( à partir de son symbole, sa date et son type)
	 * @param market Le marché recherché
	 * @return id du marché   
	 */
	public ObjectId findRawMarketData(RawMarketData market);
	
}
