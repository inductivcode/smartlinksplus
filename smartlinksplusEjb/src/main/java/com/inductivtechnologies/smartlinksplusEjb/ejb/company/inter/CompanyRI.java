package com.inductivtechnologies.smartlinksplusEjb.ejb.company.inter;

import java.io.IOException;
import java.util.List;

import javax.ejb.Remote;

import com.inductivtechnologies.smartlinksplusEjb.entities.market.Company;

/**
 * CompanyRI : interface distante de company
 */
@Remote
public interface CompanyRI{
	
	/**
	 * Service addAllCompanies  Méthode pour insérer toutes 
	 * les compagnies dans la base de donnée
	 */
	public void addAllCompanies() throws NullPointerException, IOException;
	
	/**
	 * Service findCompanyBySymbol : Recherche une companie par son symbole
	 * @param symbol Le symbol
	 * @return La compagnie correspondante ou null si aucune correspondance 
	*/
	public Company findCompanyBySymbol(String symbol);
	
	/**
	 * Service  findAllCompanies : Retourne toutes les compagnies
	 * @return La liste des compagnies
	*/
	public List<Company> findAllCompanies();
	
}
