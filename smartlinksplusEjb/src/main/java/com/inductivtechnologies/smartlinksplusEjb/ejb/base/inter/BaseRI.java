package com.inductivtechnologies.smartlinksplusEjb.ejb.base.inter;

import java.util.List;

import javax.ejb.Remote;

/**
 * BaseRI : interface de base distante(remote)
 */
@Remote
public interface BaseRI {

	/**
	 * méthode générique pour ajouter une 
	 * entité dans la base de donnée
	 * @param T l'entité en question
	*/
	public <T> void add(T t);
	
	/**
	 * méthode générique pour mettre a jour une 
	 * entité dans la base de donnée
	 * @param T l'entité en question
	 */
	public <T> void update(T t);
	
	/**
	 * méthode générique pour mettre a jour  une 
	 * entité dans la base de donnée
	 * avec selon un critere S
	 * @param T la classe de l'objet
	 * @param S le critère
	 */
	public <T,S> void updateByCriteria(T t,S s);
	
	/**
	 * méthode générique pour récuperer tous les 
	 * entités T dans la base de donnée
	 * @param T la classe de l'objet
	 * @return List<T> des entités T
	*/
	public  <T> List<T> findAll(Class<T> t);
	
	/**
	 * méthode générique pour supprimer une 
	 * entité dans la base de donnée
	 * @param T l'entité en question
	 */
	public <T> void remove(T t);
	
}
