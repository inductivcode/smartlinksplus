package com.inductivtechnologies.smartlinksplusEjb.ejb.account.impl;

import javax.ejb.Stateless;

import org.mongodb.morphia.query.Query;

import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.UserLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.UserRI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.base.impl.BaseDao;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Credential;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.User;
import com.inductivtechnologies.smartlinksplusEjb.utils.CryptoUtils;

/**
 * UserDao : Implémentation de l'interface UserRI et UserLI
*/
@Stateless(mappedName = "userDao")
public class UserDao extends BaseDao implements UserRI, UserLI{

	/**
	 * Constructeur 
	 */
	public UserDao() {
		super();
	}

	@Override
	public User findUserByCredentials(Credential credential) {
		User user = findUserByUsername(credential.getUsername());
		
		if(user != null && 
				CryptoUtils.checkPassword(credential.getPassword(), 
						user.getCredentials().getPassword())) return user;
		
		return null;
	}

	@Override
	public User findUserByUsername(String username) {
		Query<User> query = mongoDatastore.find(User.class)
				.field("credential.username").equal(username);
	
		if(query.asList().size() == 1) return query.asList().get(0);
		return null;
	}
	
}
