package com.inductivtechnologies.smartlinksplusEjb.ejb.market.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.FindOptions;
import org.mongodb.morphia.query.Query;

import com.inductivtechnologies.smartlinksplusEjb.ejb.base.impl.BaseDao;
import com.inductivtechnologies.smartlinksplusEjb.ejb.base.inter.BaseLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.FxSpotSymbolLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.RawMarketDataLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.RawMarketDataRI;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.Company;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusEjb.morphia.MongoDB;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.api.InvestingComFinance;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.api.YahooFinanceApi;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.readers.CSVReader;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.writers.RemoteDataWriter;
import com.inductivtechnologies.smartlinksplusEjb.utils.Constants;
import com.inductivtechnologies.smartlinksplusEjb.utils.DateUtils;
import com.inductivtechnologies.smartlinksplusEjb.utils.HtmlScrapper;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * RawMarketDataDao : Implémentation de l'interface RawMarketDataRI et RawMarketDataLI
 **/
@Stateless(mappedName = "rawMarketDataDAO")
public class RawMarketDataDao extends BaseDao implements RawMarketDataLI, RawMarketDataRI {

	/**
	 * MongoDb EntityManager utilisé pour l'accès au sgbd mongodb
	 */
	private final Datastore mongoDatastore;
	
	/**
	 * Fournisseur de données YAHOO
	 */
	private final YahooFinanceApi yahooFinanceApi;
	
	/**
	 * Fournisseur de données Investing.com
	 */
	private final InvestingComFinance investingComFinance;
	
	/**
	 * fxSpotSymbolDAO Accès aux données 
	 */
	@EJB
	private FxSpotSymbolLI fxSpotSymbolDao;
	
	/**
	 * baseDao Accès aux données 
	 */
	@EJB
	private BaseLI baseDao;
	
	/**
	 * Constructeur 
	 */
	public RawMarketDataDao() {
		this.mongoDatastore = MongoDB.instance().getDatabase();
		this.yahooFinanceApi = new YahooFinanceApi();
		this.investingComFinance = new InvestingComFinance();
	}
	
	//Getter(s)
	public Datastore getMongoDatastore() {
		return mongoDatastore;
	}

	@Override
	public void insertRawMarketData(RawMarketData marketData) {
		mongoDatastore.save(marketData);
	}
	
	@Override
	public void updateRawMarketData(RawMarketData marketData) {
		ObjectId idMarket = findRawMarketData(marketData);
		
		if(idMarket != null){
			marketData.setIdRawMarketData(idMarket);
			mongoDatastore.merge(marketData);
		}else{
			mongoDatastore.save(marketData);
		}
	}

	@Override
	public List<RawMarketData> rawMarketDataBetweenPeriod(String symbol, String type, 
			Date startDate, Date endDate, int skip, int limit) {
		
		Query<RawMarketData> query = mongoDatastore.find(RawMarketData.class)
				 .field("symbol").equal(symbol)
				 .field("type").equal(type);
		
		query.and(query.criteria("retrievedDate").greaterThanOrEq(startDate), 
				  query.criteria("retrievedDate").lessThanOrEq(endDate));
		
		//On trie par date décroissante
		query.order("-retrievedDate");
				
		return query.asList(new FindOptions().skip(skip).limit(limit));
	}

	@Override
	public List<RawMarketData> rawMarketDataBySymbolAndType(String symbol, String type, int skip, int limit) {
		Query<RawMarketData> query = mongoDatastore.find(RawMarketData.class)
				   .field("symbol").equal(symbol)
				   .field("type").equal(type);
		
		//On trie par date décroissante
		query.order("-retrievedDate");
		
		return query.asList(new FindOptions().skip(skip).limit(limit));
	}
	
	@Override
	public void allOldEquities(){
		//Les symboles 
//		List<Company> companies = new BaseDao().findAll(Company.class);
//		List<String> symbols = Util.allEquitiesSymbolsFromCSVFile(Constants.CSV_FILE_PATH_EQUITIES_SYMBOLS);
		List<Company> companies = baseDao.findAll(Company.class);
		
		if(!companies.isEmpty()){
			DateTime endDate = DateTime.now();
	        DateTime startDate = DateUtils.parseJodaTimeDate("2005-01-01", Constants.DATA_FORMAT_DATE);
	        
	        System.out.println("Téléchargement des données depuis 2005 ...");
	        System.out.println("Récupération du crumb ");
	        
	        String crumbCode = new String("");
	        
			for(Company company : companies){
				 if(crumbCode.isEmpty()){
					crumbCode = yahooFinanceApi.getCrumb(company.getSymbol());
					System.out.println("Crumb récupéré : " +  crumbCode);
				 }
				
				 if(crumbCode != null && !crumbCode.isEmpty()) {
					System.out.println("***************************************************");
					System.out.println("Téléchargement pour " + company.getSymbol());
					 
			        String resultFile = yahooFinanceApi
			        		.downloadEquitiesData(company.getSymbol(),
			        				DateUtils.yahooPeriod(startDate), 
			        				DateUtils.yahooPeriod(endDate),
			        				crumbCode);
			        
			        System.out.println("Téléchargement terminé");
			        
			        if(resultFile != null){
			        	List<RawMarketData> results = CSVReader
					        		.readYahooCSVFile(resultFile, company.getSymbol(), Constants.EQUITY_TYPE);
			        	
						System.out.println("Resultat pour " + company.getSymbol() + " : " + results.size() + " enrégistrements ");
						
						if(!results.isEmpty()){
							System.out.println("Enregistrement des marchés ...");
							//On on enregistre
							insertListOfRawMarketData(results);
							System.out.println("Enregistrement terminé");
						}
			        }
			     }else{
			        System.out.println(String.format("Téléchargement pour le symbole %s non abouti", company.getSymbol()));
			     }
			}
			
			System.out.println("***************************************************");
			System.out.println("Téléchargement des equities terminé");
		}else{
			System.out.println("Pas de symbole");
		}
	}
	
	@Override
	public void dailyUpdateEquities(){
		//Les symboles 
		List<Company> companies = baseDao.findAll(Company.class);
		
		if(!companies.isEmpty()){
			DateTime startDate = DateTime.now().minusDays(1);
			DateTime endDate = DateTime.now().plusDays(1);
	        
	        System.out.println("Mise à jour des equities du  " + 
	        							DateUtils.printJodaTimeDate(startDate, 
	        												Constants.DATA_FORMAT_DATE));
	        System.out.println("Récupération du crumb ");
	        
	        String crumbCode = new String("");
	        List<RawMarketData> results = null;
			for(Company company : companies){
				 if(crumbCode.isEmpty()){
					crumbCode = yahooFinanceApi.getCrumb(company.getSymbol());
					System.out.println("Crumb récupéré : " +  crumbCode);
				 }
				
				 if(crumbCode != null && !crumbCode.isEmpty()) {
			        
					System.out.println("***************************************************");
					System.out.println("Téléchargement pour " + company.getSymbol());
					 
			        String resultFile = yahooFinanceApi
			        		.downloadEquitiesData(company.getSymbol(), 
			        				DateUtils.yahooPeriod(startDate), 
			        				DateUtils.yahooPeriod(endDate), 
			        				crumbCode);
			        
			        System.out.println("Téléchargement terminé");
			        
			        if(resultFile != null){
			        	results = CSVReader
					        		.readYahooCSVFile(resultFile, company.getSymbol(), Constants.EQUITY_TYPE);
			        	
						System.out.println("Resultat pour " + company.getSymbol() + " : " + results.size() + " enrégistrements ");
						
						if(!results.isEmpty()){
							System.out.println("Enregistrement des marchés ...");
							//On met a jour 
							updateListOfRawMarketData(results);
							System.out.println("Enregistrement terminé");
						}
			        }
			     }else{
			        System.out.println(String.format("Téléchargement pour le symbole %s non abouti", company.getSymbol()));
			     }
			}
			
			System.out.println("***************************************************");
			System.out.println("Mise à jour des equities terminée");
		}else{
			System.out.println("Pas de symbole");
		}
	}
	
	@Override
	public void allOldFxSpot(){
		//Les symboles 
		List<FxSpotSymbol> symbols = fxSpotSymbolDao.allFxSpotSymbol();
//		List<FxSpotSymbol> symbols = new FxSpotSymbolDao().allFxSpotSymbol();
		
		if(!symbols.isEmpty()){
	        System.out.println("Téléchargement des données depuis 2005 ...");
	        
			for(FxSpotSymbol symbol : symbols){
				System.out.println("***************************************************");
				System.out.println("Téléchargement pour " + symbol.getSymbolValue());
				
				//Le fichier csv contenant les fx spot 
				String resultFile = Util.csvFilePathFxSpot(symbol.getSymbolValue());
						
				List<RawMarketData> results = CSVReader
		        		.readInvestingComCSVFile(resultFile, symbol.getSymbolValue(), Constants.FXSPOT_TYPE);
		        
		        System.out.println("Téléchargement terminé");
		        
		        System.out.println("Resultat pour " + symbol + " : " + results.size() + " enrégistrements ");
		        
				if(!results.isEmpty()){
					System.out.println("Enregistrement des marchés ...");
					//On enregistre
					insertListOfRawMarketData(results);
					System.out.println("Enregistrement terminé");
				}
			}
			
			System.out.println("***************************************************");
			System.out.println("Téléchargement des fx spot terminé");
		}else{
			System.out.println("Pas de symbole");
		}
	}

	@Override
	public void dailyUpdateFxSpot() {
		//Les symboles 
		List<FxSpotSymbol> symbols = fxSpotSymbolDao.allFxSpotSymbol();
	    //List<FxSpotSymbol> symbols = Util.allFxSpotSymbolsFromCSVFile(Constants.CSV_FILE_PATH_FXSPOT_SYMBOLS); 
		
		if(!symbols.isEmpty()){
			DateTime startDate = DateTime.now();
	        
	        System.out.println("Mise à jour des fx spot du  " + 
	        							DateUtils.printJodaTimeDate(startDate, 
	        												Constants.DATA_FORMAT_DATE));
	        
	        for(FxSpotSymbol symbol : symbols){
	        	 System.out.println("***************************************************");
				 System.out.println("Téléchargement pour " + symbol.getSymbolValue());
				 
				 //On télécharge la page 
	        	 String pageContent = investingComFinance.loadFxSpotPage(symbol.getSymbolValue());
	        	 
	        	 if(pageContent != null  && !pageContent.isEmpty()){
	        		 //On écrit dans le fichier html
	        		RemoteDataWriter.writeIntoAFile(Constants.FXSPOT_HTML_FILE_PATH, pageContent);
	        		
	        		//On récupère les valeurs 
	        		List<String> values = HtmlScrapper.loadLastFxSpotValues();
	        		if(!values.isEmpty()){
	        			RawMarketData market = Util.buildRawMarketDataFxSpot((Util.listToArray(values)));
	        			if(market != null){
	        				market.setSymbol(symbol.getSymbolValue());
	        				market.setType(Constants.FXSPOT_TYPE);
	        				
	        				//On affiche un peu pour vérifier 
	        				Util.viewMarket(market);
	        				
							System.out.println("Enregistrement du marché ...");
	        				//On met a jour 
							updateRawMarketData(market);
							System.out.println("Enregistrement terminé");
	        			}
	        		}
	        	 }else{
	        		 System.out.println(String.format("Téléchargement pour le symbole %s non abouti", symbol));
	        	 }
	        }
	        
	        System.out.println("***************************************************");
			System.out.println("Mise à jour des Fx spot terminée");
		}else{
			System.out.println("Pas de symbole");
		}
	}
	
	@Override
	public void insertListOfRawMarketData(List<RawMarketData> markets){
		for(RawMarketData market : markets){
			insertRawMarketData(market);
		}
	}
	
	@Override
	public void updateListOfRawMarketData(List<RawMarketData> markets){
		for(RawMarketData market : markets){
			updateRawMarketData(market);
		}
	}

	@Override
	public ObjectId findRawMarketData(RawMarketData market){
		Query<RawMarketData> query = mongoDatastore.find(RawMarketData.class)
					.field("symbol").equal(market.getSymbol())
				    .field("type").equal(market.getType());
		
		query.filter("retrievedDate =", market.getRetrievedDate());
		
		if(query.asList().size() == 1) return query.asList().get(0).getIdRawMarketData();
		return null;
	}

}

