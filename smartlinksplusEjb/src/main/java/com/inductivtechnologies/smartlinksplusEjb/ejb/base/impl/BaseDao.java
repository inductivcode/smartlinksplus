package com.inductivtechnologies.smartlinksplusEjb.ejb.base.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;

import com.inductivtechnologies.smartlinksplusEjb.ejb.base.inter.BaseLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.base.inter.BaseRI;
import com.inductivtechnologies.smartlinksplusEjb.morphia.MongoDB;

/**
 * BaseDao : Dao basique pour les méthodes usuelles
*/
@Stateless(mappedName = "baseDao")
public class BaseDao implements BaseLI, BaseRI {

	/**
	 * datastore représentant l'instance de la base de donnée
	 */
	protected final Datastore mongoDatastore;
	
	/**
	 * Constructeur 
	 */
	public BaseDao(){
		super();
		mongoDatastore = MongoDB.instance().getDatabase();
	}
	
	@Override
	public <T> void add(T t) {
		mongoDatastore.save(t);
	}

	@Override
	public <T> void update(T t){
		mongoDatastore.merge(t);
	}

	@Override
	public <T, S> void updateByCriteria(T t, S s) {
	}

	@Override
	public <T> List<T> findAll(Class<T> t){
		Query<T> quer = mongoDatastore.find(t);
		List<T> ls = new ArrayList<>();
		
		if(null != quer){
			ls = quer.asList();
		}
		
		return ls;
	}

	@Override
	public <T> void remove(T t){
		mongoDatastore.delete(t);
	}
}
