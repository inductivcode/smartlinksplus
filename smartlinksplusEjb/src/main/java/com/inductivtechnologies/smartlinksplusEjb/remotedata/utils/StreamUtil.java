package com.inductivtechnologies.smartlinksplusEjb.remotedata.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * StreamUtil : Regroupe les méthodes qui permettent de lire des données 
 * brutes provenant d'une source distante (HTTP)
 */
public class StreamUtil {

    /**
     * Lit un flux de données caractère par caractère 
     * @param inputStream Le flux de données
     * @return Retourne un String qui est la représentation en chaine de caractères du flux de données
     */
	 public static String readRawStream(InputStream inputStream) {
	    String stringContent = null;
	    BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
     	
     	int bytesRead = 0;
        byte[] contents = new byte[1024];
         
        try{
			while((bytesRead = bufferedInputStream.read(contents)) != -1) { 
				stringContent += new String(contents, 0, bytesRead);              
			}
		}catch(IOException exception) {
			System.out.println("Lecture du flux non aboutie");
			System.out.println("IOException");
	        System.out.println(exception);
		}
         
        return stringContent;
	 }
	 
	 /**
	 * Lit un flux de données en ligne
	 * @param inputStream Le flux de données
	 * @return Retourne un String qui est la représentation en chaine de caractères du flux de données
	 */
	 public static String readLinesStream(InputStream inputStream) {
   	  	BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	    StringBuffer result = new StringBuffer();
	    String line = "";
	      
	    try {
			while((line = bufferedReader.readLine()) != null) {
			      result.append(line);
			}
		}catch(IOException exception){
			System.out.println("Lecture du flux non aboutie");
			System.out.println("IOException");
	        System.out.println(exception);
		}
	    
	    return result.toString();
	 }
}
