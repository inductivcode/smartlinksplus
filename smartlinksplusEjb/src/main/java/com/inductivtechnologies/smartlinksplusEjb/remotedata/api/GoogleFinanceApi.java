package com.inductivtechnologies.smartlinksplusEjb.remotedata.api;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;

import com.inductivtechnologies.smartlinksplusEjb.remotedata.utils.StreamUtil;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.writers.RemoteDataWriter;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * GoogleFinanceApi : Regroupe les méthodes qui permettent de récupérer
 *  les données depuis l'api GOOGLE finance
 */
public class GoogleFinanceApi {
	
	private static final String DOWNLOAD_EQUITIES_DATA_URL = "https://finance.google.com/finance/historical?" 
			+ "q=%s&startdate=%s&output=csv";
	
	/**
	 * context : Utilisé pour gérer l'exécution des requêtes HTTP
	 */
	private HttpClientContext httpContext;
	
	/**
	 * httpClient : Utilisé pour l'exécution des requêtes HTTP spécifiques
	 */
	private HttpClient httpClient;
	
	/**
	 * TIME_OUT : Temps d'attente
	 */
	private final int TIME_OUT = 2; //5 secondes
	
	/**
     * Constructeur
     */
	public GoogleFinanceApi() {
	   super();
	   
	   CookieStore cookieStore = new BasicCookieStore();

	   httpClient = HttpClientBuilder
			   			.create().setDefaultRequestConfig(this.configHttpClient()).build();
	   httpContext = HttpClientContext.create();
	   httpContext.setCookieStore(cookieStore);
	}
	
	public RequestConfig configHttpClient(){
	  return  RequestConfig.custom()
				.setConnectTimeout(TIME_OUT * 10000)
				.setConnectionRequestTimeout(TIME_OUT * 10000)
				.setSocketTimeout(TIME_OUT * 1000).build();
	}
	
	/**
     * Intérroge l'api pour récupérer les equities 
     * 
     * @param symbol Le symbole de l'entreprise
     * @param startDate Date min
     * @param endDate Date max
     * @param crumb Code 
     */
	public void downloadEquitiesData(String symbol, String startDate) {
		String csvFile = Util.csvFilePathEquities(symbol);
	        
	    String url = String.format(DOWNLOAD_EQUITIES_DATA_URL, symbol, startDate);
	    HttpGet request = new HttpGet(url);
	    
	    System.out.println("Appel de l'api Google finance afin de  charger les equities ..");
	    System.out.println(url);
	    
	    try{
	         HttpResponse response = httpClient.execute(request, httpContext);
	         int statusCode = response.getStatusLine().getStatusCode();
	         String reasonPhrase = response.getStatusLine().getReasonPhrase();
	         
	         System.out.println("Chargement terminé avec le code : " + statusCode);
	         System.out.println("Phrase du chargement  : " + reasonPhrase);
	         
	         HttpEntity entity = response.getEntity();
	    	  
	    	 if(entity != null){
		        String fileContent = StreamUtil.readRawStream(entity.getContent());
		            	
		        if(fileContent != null){
		            if(statusCode == 200){
		            	//Ecriture 
		            	RemoteDataWriter.writeIntoAFile(csvFile, fileContent);
			         }else{
			        	 System.out.println(fileContent);		
			         }
	         	}else{
	         		System.out.println("Lecture du flux de données HTTP non aboutie");
	         	}
		      }
		      HttpClientUtils.closeQuietly(response);

	        } catch (Exception exception) {
	            System.out.println("Chargement non abouti");
	            System.out.println("Exception");
	            System.out.println(exception);
	        }
	}
	
	public HttpClientContext getHttpContext() {
		return httpContext;
	}

	public void setHttpContext(HttpClientContext httpContext) {
		this.httpContext = httpContext;
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
}
