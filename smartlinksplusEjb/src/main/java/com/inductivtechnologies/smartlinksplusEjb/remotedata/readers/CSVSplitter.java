package com.inductivtechnologies.smartlinksplusEjb.remotedata.readers;

import java.util.ArrayList;
import java.util.List;

/**
 * CSVSplitter : Décompose les lignes d'un fichier CSV (Comma Separated Values)
 */
public class CSVSplitter {
	
	/** 
	 * Le séparateur des champs. Par défaut c'est ','. 
	 */
	private char fieldSeperator;

	/**
	 * Le caractère de citation. La valeur par défaut est '"'. Ex: 12345,"Builder,
	 * Bob",BUILDER,07 389 3896.
	 */
	private final char quoteCharacter;

	/**
	 * Si true alors "cat,,,dog" est équivalent à "cat,dog". La valeur par défaut est false.
	 */
	private final boolean treatConsecutiveSeperatorsAsOne;

	/**
	 * Si true alors "cat, dog" est équivalent à  "cat,dog". La valeur par défaut est true.
	 */
	private final boolean trimFields;

	/**
	 * Initialisation avec les valeurs par défaut field-seperator=','
	 * quoteCharacter='"' treatConsecutiveSeperatorsAsOne=false trimFields=true
	 */
	public CSVSplitter(){
		this(',', '"', false, true);
	}
	
	/**
	 * Initialise le CSVSplitter.
	 * 
	 * @param fieldSeperator
	 *            char - Le séparateur des champs. Par défaut c'est ','.
	 * @param quoteCharacter
	 *            char - Le caractère de citation. La valeur par défaut est '"'.
	 * @param treatConsecutiveSeperatorsAsOne
	 *            boolean - Si true alors "cat,,,dog" est équivalent à "cat,dog". La valeur par défaut est false.
	 * @param trimFields
	 *            boolean - Si true alors "cat, dog" est équivalent à  "cat,dog". La valeur par défaut est true.
	 */
	public CSVSplitter(char fieldSeperator, char quoteCharacter, boolean treatConsecutiveSeperatorsAsOne,
			boolean trimFields) {
		this.fieldSeperator = fieldSeperator;
		this.treatConsecutiveSeperatorsAsOne = treatConsecutiveSeperatorsAsOne;
		this.quoteCharacter = quoteCharacter;
		this.trimFields = trimFields;
	}
	
	/**
	 * Décompose une ligne du fichier CSV
	 * 
	 * @param line
	 *            String - Une ligne du fichier CSV.
	 * @return String[] - Le résultat de la décomposition
	 */
	public String[] split(String line) {
		List<String> tokens = new ArrayList<String>();
		char[] characters = line.toCharArray(); 
		
		int n = characters.length;
		boolean quoted = false;
		StringBuilder token = new StringBuilder();
		for (int i = 0; i < n; i++) { 
			char character = characters[i];

			char next = i + 1 == n ? '\0' : characters[i + 1];
			
			boolean discard = false;

			if (character == quoteCharacter) {
				if (!quoted) {
					quoted = true;
					discard = true;
				}else{
					if(next == quoteCharacter) {
						token.append(quoteCharacter);
						i++;
						continue;
					}else{
						quoted = false;
						discard = true;
					}
				}

			}else if(character == fieldSeperator) {
				if (!quoted) {
					if (treatConsecutiveSeperatorsAsOne && token.length() == 0) {
						discard = true;
					}else{
						tokens.add(asString(token));
						token.setLength(0);
						discard = true;
					}
				}
			}
			
			if (!discard) {
				token.append(character);
			}
		}

		String strtok = asString(token);

		if(!(tokens.isEmpty() && strtok.length() == 0)) {
			tokens.add(strtok);
		}

		return tokens.toArray(new String[0]);
	}
	
	private String asString(StringBuilder token) {
		String strtok = token.toString();
		return (trimFields ? strtok.trim() : strtok);
	}

	public char getFieldSeperator() {
		return fieldSeperator;
	}

	public void setFieldSeperator(char fieldSeperator) {
		this.fieldSeperator = fieldSeperator;
	}

	public char getQuoteCharacter() {
		return quoteCharacter;
	}

	public boolean isTreatConsecutiveSeperatorsAsOne() {
		return treatConsecutiveSeperatorsAsOne;
	}

	public boolean isTrimFields() {
		return trimFields;
	}
}
