package com.inductivtechnologies.smartlinksplusEjb.remotedata.api;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;

import com.inductivtechnologies.smartlinksplusEjb.remotedata.utils.StreamUtil;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.writers.RemoteDataWriter;
import com.inductivtechnologies.smartlinksplusEjb.utils.Constants;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * YahooFinanceApi : Regroupe les méthodes qui permettent de récupérer
 *  les données depuis l'api YAHOO finance
 */
public class YahooFinanceApi {
	
	private static final String CRUMB_URL = "https://finance.yahoo.com/quote/%s/?p=%s";
	
	private static final String DOWNLOAD_EQUITIES_DATA_URL = "https://query1.finance.yahoo.com/v7/finance/download/"
			+ "%s?period1=%s&period2=%s&interval=1d&events=history&crumb=%s";
	
	private static final String BROWSER_USER_AGENT = "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.13) "
			+ "Gecko/20101206 Ubuntu/10.10 (maverick) Firefox/3.6.13";

	/**
	 * context : Utilisé pour gérer l'exécution des requêtes HTTP
	 */
	private HttpClientContext httpContext;
	
	/**
	 * httpClient : Utilisé pour l'exécution des requêtes HTTP spécifiques
	 */
	private HttpClient httpClient;
	
	/**
	 * TIME_OUT : Temps d'attente
	 */
	private final int TIME_OUT = 120;
	
	/**
     * Constructeur
     */
	public YahooFinanceApi() {
	   super();
	   
	   CookieStore cookieStore = new BasicCookieStore();

	   httpClient = HttpClientBuilder
			   			.create().setDefaultRequestConfig(this.configHttpClient()).build();
	  
	   httpContext = HttpClientContext.create();
	   httpContext.setCookieStore(cookieStore);
	}
	
	public RequestConfig configHttpClient(){
	  return  RequestConfig.custom()
				.setConnectTimeout(TIME_OUT * 1000)
				.setConnectionRequestTimeout(TIME_OUT * 1000)
				.setSocketTimeout(TIME_OUT * 1000).build();
	}
	
	/**
     * Charge la page contenant le code crumb
     *
     * @param symbol Le symbole de l'entreprise 
     * @return Retourne un String représentant le contenu de la page 
     */
	public String loadCrumbPage(String symbol) {
	   String pageContent = null;
	   
	   String url = String.format(CRUMB_URL, symbol, symbol);
	   HttpGet request = new HttpGet(url);
	   
	   System.out.println("Chargement de la page du crumb ...");

	   //On simule un browser 
	   request.addHeader("User-Agent", BROWSER_USER_AGENT);
	   
	   try{
	      HttpResponse response = httpClient.execute(request, httpContext);
	      int statusCode = response.getStatusLine().getStatusCode();
	      String reasonPhrase = response.getStatusLine().getReasonPhrase();
	      
	      System.out.println("Chargement terminé avec le code : " + statusCode);
	      System.out.println("Phrase du chargement  : " + reasonPhrase);
	      
	      if(statusCode == 200){
	    	  HttpEntity entity = response.getEntity();
		      
		      if(entity != null){
			      pageContent = StreamUtil.readLinesStream(entity.getContent());
			      HttpClientUtils.closeQuietly(response);
		      }
	      }else{
	    	  System.out.println("Chargement échoué avec le code : " + statusCode);
	      }
	   }catch (Exception exception) {
	      System.out.println("Exception levée");
	      System.out.println(exception);
	   }
	   
	   return pageContent;
	}
	
	/**
     * Recherche le Crumb(code utilisé pour faire les appels à l'api)
     *
     * @param lines Chaines de caractères contenant le crumb
     * @return Retourne un String représentant le code utilisé pour faire les appels à l'api
     */
	public String findCrumb(List<String> lines) {
	   String crumb = "";
	   String crumbLine = "";
	   
	   if(lines != null){
		   for(String line : lines) {
			  if(line.indexOf(Constants.CRUMB_LABEL) > -1) {
			      crumbLine = line;
			      break;
			  }
		   }
		   //"CrumbStore":{"crumb":"OKSUqghoLs8"}     
		   if(crumbLine != null && !crumbLine.isEmpty()) {
			    String[] vals = crumbLine.split(":");
			    //On récupère le troisième élément ie "OKSUqghoLs8"
			    //Puis on remplace " par le vide ""
			    crumb = vals[2].replace("\"", "");
			    //On élimine les caractères spéciaux
			    crumb = StringEscapeUtils.unescapeJava(crumb);
		    }
	   }
	   
	   return crumb;
	}
	
	/**
     * Renvoie le code crumb
     * 
     * @param symbol Le symbole de l'entreprise
     * @return Retourne un String représentant le crumb code
     */
	public String getCrumb(String symbol){
		String pageContent = loadCrumbPage(symbol);
		
		if(pageContent != null && !pageContent.isEmpty())
			return findCrumb(splitPageData(pageContent));
	    return null;
	}
	
	/**
     * Renvoie la page en tant que liste de chaînes de caractères en 
     * utilisant le caractère "}" comme délimiteur
     *
     * @param stringPage Le contenu de la page
     * @return Retourne une List<String> représentant les chaines obtenues
     */
	public List<String> splitPageData(String stringPage) {
		if(stringPage != null) return Arrays.asList(stringPage.split("}"));
	    return null;
	}
	
	/**
     * Intérroge l'api pour récupérer les equities 
     * 
     * @param symbol Le symbole de l'entreprise
     * @param startDate Date min
     * @param endDate Date max
     * @param crumb Code 
     * @return Le chemin vers le fichier contenant les données
     */
	public String downloadEquitiesData(String symbol, long startDate, long endDate, String crumb) {
		String csvFile = null;
	        
	    String url = String.format(DOWNLOAD_EQUITIES_DATA_URL, symbol, startDate, endDate, crumb);
	    HttpGet request = new HttpGet(url);
	    
	    System.out.println("URL demandé : " + url);

	    request.addHeader("User-Agent", BROWSER_USER_AGENT);
	    
	    try{
	         HttpResponse response = httpClient.execute(request, httpContext);
	         int statusCode = response.getStatusLine().getStatusCode();
	         String reasonPhrase = response.getStatusLine().getReasonPhrase();
	         
	         System.out.println("Chargement terminé avec le code : " + statusCode);
	         System.out.println("Phrase du chargement  : " + reasonPhrase);
	         
	         HttpEntity entity = response.getEntity();
	    	  
	    	 if(entity != null){
		        String fileContent = StreamUtil.readRawStream(entity.getContent());
		            	
		        if(fileContent != null){
		            if(statusCode == 200){
		            	csvFile = Util.csvFilePathEquities(symbol);
		            	//Ecriture 
		            	RemoteDataWriter.writeIntoAFile(csvFile, fileContent);
			         }else{
			        	 System.out.println("Sans doute aucune données pour le symbol : " + symbol);	
			        	 System.out.println(fileContent);		
			         }
	         	}else{
	         		System.out.println("Lecture du flux de données HTTP non aboutie");
	         	}
		      }
		      HttpClientUtils.closeQuietly(response);

	        } catch (Exception exception) {
	            System.out.println("Chargement non abouti");
	            System.out.println("Exception");
	            System.out.println(exception);
	        }
	    
	    return csvFile;
	}

	public HttpClientContext getHttpContext() {
		return httpContext;
	}

	public void setHttpContext(HttpClientContext httpContext) {
		this.httpContext = httpContext;
	}

	public HttpClient getHttpClient() {
		return httpClient;
	}

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
}
