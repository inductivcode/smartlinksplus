package com.inductivtechnologies.smartlinksplusEjb.remotedata.writers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.inductivtechnologies.smartlinksplusEjb.entities.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusEjb.utils.Constants;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * RemoteDataWriter : Regroupe les méthodes qui permettent d'écrire
 *  des données distantes (HTTP) dans des fichiers CSV
 */
public class RemoteDataWriter {
	
	/**
     * Ecrit des données dans un fichier
     * @param fileName Le nom du fichier 
     * @param content Le contenu à ecrire
     */
	public static void writeIntoAFile(String fileName, String content){
		FileWriter fileWriter;
		
		try{
			fileWriter = new FileWriter(fileName);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			
			System.out.println("Ecriture du fichier patientez ...");
			printWriter.write(content);
			printWriter.close();
	        System.out.println("Ecriture terminée");
	        
		}catch(IOException exception) {
			 System.out.println("Ecriture non aboutie");
			 System.out.println("IOException");
	         System.out.println(exception);
		}
	}
	
	/**
     * Ecrit des données dans un fichier CSV
     * @param symbol Le symbole correspondant aux données
     * @param markets Le contenu à ecrire
     * @param columns Les titres des colonnes du fichier
     * @param type Le type de données à écrire
     * @return File Le fichier résultant 
     */
	public static File writeDatatIntoCSVFile(String symbol, List<RawMarketData> markets, String[] columns, String type){
		String csvFileName = String.format(Constants.DOWNLOAD_FILE_PATH, symbol);
		boolean createFileResult = Util.createFileIfNotExist(csvFileName);
		
		//Le fichier 
		File file = null;
		FileWriter fileWriter;
		
		if(createFileResult){
			System.out.println("Fichier Demandé : " + csvFileName);
			file = new File(csvFileName);
			try {
				fileWriter = new FileWriter(file);
				PrintWriter printWriter = new PrintWriter(fileWriter);
				
				//Le contenu du fichier 
				StringBuilder fileContent = new StringBuilder();
				//Le contenu des colonnes 
				StringBuilder strColumns = new StringBuilder();
				
				System.out.println("Ecriture du fichier patientez ...");
				
				//On construit les colonnes 
				for(int i= 0; i < columns.length; i++){
					strColumns.append(columns[i]);
					//Le séparateur 
					if(i != (columns.length - 1)) strColumns.append(",");
				}
				
				//On ajoute les colonnes 
				fileContent.append(strColumns);
				fileContent.append("\n");
				
				//On construit les données  
				for(RawMarketData market : markets){
					//On ajoute une ligne de données
					fileContent.append(Util.transformRawMarketDataToString(type, market));
					//Le retour à la ligne 
					fileContent.append("\n");
				}
				
				printWriter.write(fileContent.toString());
				printWriter.close();
				System.out.println("Ecriture terminée");
				
			}catch (IOException exception) {
				 System.out.println("Ecriture non aboutie");
				 System.out.println("IOException");
		         System.out.println(exception);
			}
		}
		
		return file;
	}
}
