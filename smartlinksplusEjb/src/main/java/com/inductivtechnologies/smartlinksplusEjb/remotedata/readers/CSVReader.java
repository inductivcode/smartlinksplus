package com.inductivtechnologies.smartlinksplusEjb.remotedata.readers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.inductivtechnologies.smartlinksplusEjb.entities.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * CSVReader : Lecture d'un fichier CSV (Comma Separated Values)
 */
public class CSVReader {
	
	/**
	 * csvSplitter : Pour décomposer l csves lignes du fichier
	 */
	private static final CSVSplitter csvSplitter = new CSVSplitter(',', '"', true, true);

	/**
	 * Lit un fichier CSV contenant des symboles pour les equities (GOOG,AAPL)
	 * @param csvFile - Le nom du fichier 
	 * @return List<String> : La liste des symboles
	 */
	public static List<String> allEquitiesSymbols(String csvFile){
		List<String> symbols = new ArrayList<>();
	
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;

		try{
			//bufferedReader = new BufferedReader(new FileReader(FILENAME));
			fileReader = new FileReader(csvFile);
			bufferedReader = new BufferedReader(fileReader);
			
			System.out.println("Lecture des symboles ...");
			
			if(bufferedReader.ready()){
				String firstLine = bufferedReader.readLine();
				
				if(!firstLine.isEmpty()){
					String nextLine;

					while ((nextLine = bufferedReader.readLine()) != null){
						
						csvSplitter.setFieldSeperator(';');
						String[] csvColumns = csvSplitter.split(nextLine);
						
						if(csvColumns.length != 0){
							symbols.add(csvColumns[0]);
						}
					}
					
					System.out.println("Lecture terminée");
				}
			}
		}catch(IOException exception) {
			 System.out.println("Lecture non aboutie");
			 System.out.println("IOException");
	         System.out.println(exception);

		}finally{
			try{
				if(bufferedReader != null)
					bufferedReader.close();

				if(fileReader != null)
					fileReader.close();
				
				System.out.println("Fichier fermé");
			}catch(IOException exception) {
				System.out.println("Fichier non fermé");
				System.out.println("IOException");
		        System.out.println(exception);
			}
		}
		
		return symbols;
	}
	
	/**
	 * Lit un fichier CSV contenant des symboles (EURGBP, EURUSD)
	 * @param csvFile - Le nom du fichier 
	 * @return List<String> : La liste des symboles
	 */
	public static List<FxSpotSymbol> allFXSpotSymbols(String csvFile){
		List<FxSpotSymbol> fxSpotSymbols = new ArrayList<>();
	
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;

		try{
			fileReader = new FileReader(csvFile);
			bufferedReader = new BufferedReader(fileReader);
			
			System.out.println("Lecture des symboles ...");
			
			if(bufferedReader.ready()){
				String nextLine;

				while ((nextLine = bufferedReader.readLine()) != null){
					
					csvSplitter.setFieldSeperator(',');
					String[] csvColumns = csvSplitter.split(nextLine);
					//System.out.println(nextLine);
					
					if(csvColumns.length == 2){
						FxSpotSymbol fxSpotSymbol = new FxSpotSymbol();
						fxSpotSymbol.setSymbolValue(csvColumns[0]);
						fxSpotSymbol.setSymbolDescription(csvColumns[1]);
						
						fxSpotSymbols.add(fxSpotSymbol);
					}
				}
				
				System.out.println("Lecture terminée");
			}
		}catch(IOException exception) {
			 System.out.println("Lecture non aboutie");
			 System.out.println("IOException");
	         System.out.println(exception);

		}finally{
			try{
				if(bufferedReader != null)
					bufferedReader.close();

				if(fileReader != null)
					fileReader.close();
				
				System.out.println("Fichier fermé");
			}catch(IOException exception) {
				System.out.println("Fichier non fermé");
				System.out.println("IOException");
		        System.out.println(exception);
			}
		}
		
		return fxSpotSymbols;
	}
	
	/**
	 * Lit un fichier CSV provenant de yahoo finance 
	 * @param fileName - Le nom du fichier 
	 * @param symbol  - Le symbole pour lequel on lit les données
	 * @param typeOfMarket - Le type des données à lire
	 * @return List<RawMarketData> : Une liste de valeurs de marché 
	 */
	public static List<RawMarketData> readYahooCSVFile(String csvFile, String symbol, String typeOfMarket){
		List<RawMarketData> dataResult = new ArrayList<>();
		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;

		try{
			fileReader = new FileReader(csvFile);
			bufferedReader = new BufferedReader(fileReader);
			
			System.out.println("Lecture du fichier " + csvFile + " patientez ...");
			
			if(bufferedReader.ready()){
				String firstLine = bufferedReader.readLine();
				
				if(!firstLine.isEmpty()){
					String nextLine;

					while ((nextLine = bufferedReader.readLine()) != null){
						csvSplitter.setFieldSeperator(',');
						String[] csvColumns = csvSplitter.split(nextLine);
						
						if(csvColumns.length == 7){
							//Construction du marché 
							RawMarketData marketData = Util.buildRawMarketDataEquities(csvColumns);
							if(marketData != null){
								marketData.setType(typeOfMarket);
								marketData.setSymbol(symbol);
								dataResult.add(marketData);
							}
						}
					}
					
					System.out.println("Lecture terminée");
				}
			}
		}catch(IOException exception) {
			 System.out.println("Lecture non aboutie");
			 System.out.println("IOException");
	         System.out.println(exception);
		}finally{
			try{
				if(bufferedReader != null)
					bufferedReader.close();

				if(fileReader != null)
					fileReader.close();
				
				System.out.println("Fichier fermé");
			}catch(IOException exception) {
				System.out.println("Fichier non fermé");
				System.out.println("IOException");
		        System.out.println(exception);
			}
		}
		
		return dataResult;
	}

	/**
	 * Lit un fichier CSV provenant de investing.com
	 * @param fileName  - Le nom du fichier 
	 * @param symbol  - Le symbole pour lequel on lit les données
	 * @param typeOfMarket - Le type des données à lire
	 * @return List<RawMarketData> : Une liste de valeurs de marché 
	 */
	public static List<RawMarketData> readInvestingComCSVFile(String csvFile, String symbol, String typeOfMarket){
		List<RawMarketData> dataResult = new ArrayList<>();
		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;

		try{
			fileReader = new FileReader(csvFile);
			bufferedReader = new BufferedReader(fileReader);
			
			System.out.println("Lecture du fichier " + csvFile + " patientez ...");
			
			if(bufferedReader.ready()){
				String firstLine = bufferedReader.readLine();
				
				if(!firstLine.isEmpty()){
					String nextLine;

					while ((nextLine = bufferedReader.readLine()) != null){
						csvSplitter.setFieldSeperator(',');
						String[] csvColumns = csvSplitter.split(nextLine);
						
						if(csvColumns.length == 6){
							//Construction du marché 
							RawMarketData marketData = Util.buildRawMarketDataFxSpot(csvColumns);
							if(marketData != null){
								marketData.setType(typeOfMarket);
								marketData.setSymbol(symbol);
								dataResult.add(marketData);
							}
						}
					}
					
					System.out.println("Lecture terminée");
				}
			}
		}catch(IOException exception) {
			 System.out.println("Ecriture non aboutie");
			 System.out.println("IOException");
	         System.out.println(exception);
		}finally{
			try{
				if(bufferedReader != null)
					bufferedReader.close();

				if(fileReader != null)
					fileReader.close();
				
				System.out.println("Fichier fermé");
			}catch(IOException exception) {
				System.out.println("Fichier non fermé");
				System.out.println("IOException");
		        System.out.println(exception);
			}
		}
		
		return dataResult;
	}
	
	public CSVSplitter getCsvSplitter() {
		return csvSplitter;
	}
}
