package com.inductivtechnologies.smartlinksplusEjb.remotedata.api;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;

import com.inductivtechnologies.smartlinksplusEjb.remotedata.utils.StreamUtil;

/**
 * InvestingComFinance : Regroupe les méthodes qui permettent de récupérer
 *  les données depuis le site investing.com
 */
public class InvestingComFinance {
	
	private static final String DOWNLOAD_FXSPOT_DATA_URL = "https://www.investing.com/currencies/%s-%s-historical-data";
	
	private static final String BROWSER_USER_AGENT = "Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2.13) "
			+ "Gecko/20101206 Ubuntu/10.10 (maverick) Firefox/3.6.13";

	/**
	 * context : Utilisé pour gérer l'exécution des requêtes HTTP
	 */
	private HttpClientContext httpContext;
	
	/**
	 * httpClient : Utilisé pour l'exécution des requêtes HTTP spécifiques
	 */
	private HttpClient httpClient;
	
	/**
	 * TIME_OUT : Temps d'attente
	 */
	private final int TIME_OUT = 120;
	
	/**
     * Constructeur
     */
	public InvestingComFinance() {
	   super();
	   
	   CookieStore cookieStore = new BasicCookieStore();

	   httpClient = HttpClientBuilder
			   			.create().setDefaultRequestConfig(this.configHttpClient()).build();
	   
	   httpContext = HttpClientContext.create();
	   httpContext.setCookieStore(cookieStore);
	}
	
	public RequestConfig configHttpClient(){
	  return  RequestConfig.custom()
				.setConnectTimeout(TIME_OUT * 1000)
				.setConnectionRequestTimeout(TIME_OUT * 1000)
				.setSocketTimeout(TIME_OUT * 1000).build();
	}
	
	/**
     * Charge une page du site investing.com afin d recuperer les valeurs des fx spots 
     * @param symbol Le symbol du fxspot
     * @return String : Le contenu html sous forme de chaine de caractères 
     */
	public String loadFxSpotPage(String symbol) {
		 String pageContent = null; 
		 
		 String url = generateInvestingComFxSpotUrl(symbol);
		 HttpGet request = new HttpGet(url);
		   
		 System.out.println(url);
		 System.out.println("Chargement de la page ...");

		 request.addHeader("User-Agent", BROWSER_USER_AGENT);
		   
		 try{
		    HttpResponse response = httpClient.execute(request, httpContext);
		    int statusCode = response.getStatusLine().getStatusCode();
		    String reasonPhrase = response.getStatusLine().getReasonPhrase();
		      
		    System.out.println("Chargement terminé avec le code : " + statusCode);
		    System.out.println("Phrase du chargement  : " + reasonPhrase);
		      
		    if(statusCode == 200){
		    	HttpEntity entity = response.getEntity();
			      
			    if(entity != null){
				    pageContent = StreamUtil.readLinesStream(entity.getContent());
				    HttpClientUtils.closeQuietly(response);
			    }
			     
		    }else{
		    	System.out.println("Chargement échoué avec le code : " + statusCode);
		    }
		}catch (Exception exception) {
		   System.out.println("Exception levée");
		   System.out.println(exception);
		}
		   
		return pageContent;
	}
	
	/**
     * Génère une url en fontion du symbole 
     * @param symbol Le symbol du fxspot
     * @return String : L'url générée
     */
	private String generateInvestingComFxSpotUrl(String symbol) {
		return String.format(DOWNLOAD_FXSPOT_DATA_URL, 
				 symbol.substring(0, 3).toLowerCase(), symbol.substring(3).toLowerCase());
	}
}
