package com.inductivtechnologies.smartlinksplusEjb.entities.market;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

/**
 * ProfileComp : Le profile de la compagnie
 **/
@Embedded
public class ProfileComp implements Serializable{

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = -4088085101506537904L;

	/**
	 * Le secteur d'activité
	 */
    private String sector;
	
    /**
	 * Le type d'industrie
	 */
    private String industry;
    
    /**
	 *Indique comment les actions de la compagnie sont stoquées
	 */
    private String stockStyle;
    
    /**
	 * Le nombre d'employé
	 */
    private Double nbEmp;
    
    /**
	 * fiscalYearEnds
	 */
    private String fiscalYearEnds;
    
    /**
     * Description de la compagnie
     */
    private String description;
  
	/**
	 * Constructeur
	*/
	public ProfileComp() {
		super();
	}

	/**
	 * Constructeur
	*/
	public ProfileComp(String sector, String industry, String stockStyle, 
			Double nbEmp, String fiscalYearEnds,
			String description) {
		super();
		this.sector = sector;
		this.industry = industry;
		this.stockStyle = stockStyle;
		this.nbEmp = nbEmp;
		this.fiscalYearEnds = fiscalYearEnds;
		this.description = description;
	}

	//Getters et setters 
	
	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getStockStyle() {
		return stockStyle;
	}

	public void setStockStyle(String stockStyle) {
		this.stockStyle = stockStyle;
	}

	public Double getNbEmp() {
		return nbEmp;
	}

	public void setNbEmp(Double nbEmp) {
		this.nbEmp = nbEmp;
	}

	public String getFiscalYearEnds() {
		return fiscalYearEnds;
	}

	public void setFiscalYearEnds(String fiscalYearEnds) {
		this.fiscalYearEnds = fiscalYearEnds;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((fiscalYearEnds == null) ? 0 : fiscalYearEnds.hashCode());
		result = prime * result + ((industry == null) ? 0 : industry.hashCode());
		result = prime * result + ((nbEmp == null) ? 0 : nbEmp.hashCode());
		result = prime * result + ((sector == null) ? 0 : sector.hashCode());
		result = prime * result + ((stockStyle == null) ? 0 : stockStyle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileComp other = (ProfileComp) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (fiscalYearEnds == null) {
			if (other.fiscalYearEnds != null)
				return false;
		} else if (!fiscalYearEnds.equals(other.fiscalYearEnds))
			return false;
		if (industry == null) {
			if (other.industry != null)
				return false;
		} else if (!industry.equals(other.industry))
			return false;
		if (nbEmp == null) {
			if (other.nbEmp != null)
				return false;
		} else if (!nbEmp.equals(other.nbEmp))
			return false;
		if (sector == null) {
			if (other.sector != null)
				return false;
		} else if (!sector.equals(other.sector))
			return false;
		if (stockStyle == null) {
			if (other.stockStyle != null)
				return false;
		} else if (!stockStyle.equals(other.stockStyle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProfileComp [sector=" + sector + ", industry=" + industry + ", stockStyle=" + stockStyle + ", nbEmp="
				+ nbEmp + ", fiscalYearEnds=" + fiscalYearEnds + ", description=" + description + "]";
	}
}