package com.inductivtechnologies.smartlinksplusEjb.entities.market;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

/**
 * Financial: les états financiers de la compagnie
 **/
@Embedded
public class Financial implements Serializable{

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = -5509523261605047982L;
	
	/**
	 * turnover
	 */
	private Double turnover;
	
	/**
	 * operatingProfit
	 */
	private Double operatingProfit;
	
	/**
	 * netProfit
	 */
	private Double netProfit;	
	
	/**
	 * reportedEPS
	 */
	private Double reportedEPS;
	
	/**
	 * currentAssets
	 */
	private Double currentAssets;	
	
	/**
	 * nonCurrentAssets
	 */
	private Double nonCurrentAssets;	
	
	/**
	 * totalAssets
	 */
	private Double totalAssets;	
	
	/**
	 *currentLiabilities
	 */
	private Double currentLiabilities;	
	
	/**
	 * totalLiabilities
	 */
	private Double totalLiabilities;	
	
	/**
	 * totalEquity
	 */
	private Double totalEquity;	
	
	/**
	 * operatingCashFlow
	 */
	private Double operatingCashFlow;	
	
	/**
	 * netChangeinCash
	 */
	private Double netChangeinCash;
	
	/**
	 * category
	 */
	private String category;
	
	/**
	 * year
	 */
	private String year;

	/**
	 * Constructeur
	 */
	public Financial() {
		super();
	}

	/**
	 * Constructeur
	 */
	public Financial(Double turnover, Double operatingProfit, Double netProfit, Double reportedEPS,
			Double currentAssets, Double nonCurrentAssets, Double totalAssets, Double currentLiabilities,
			Double totalLiabilities, Double totalEquity, Double operatingCashFlow, Double netChangeinCash,
			String category, String year) {
		
		super();
		this.turnover = turnover;
		this.operatingProfit = operatingProfit;
		this.netProfit = netProfit;
		this.reportedEPS = reportedEPS;
		this.currentAssets = currentAssets;
		this.nonCurrentAssets = nonCurrentAssets;
		this.totalAssets = totalAssets;
		this.currentLiabilities = currentLiabilities;
		this.totalLiabilities = totalLiabilities;
		this.totalEquity = totalEquity;
		this.operatingCashFlow = operatingCashFlow;
		this.netChangeinCash = netChangeinCash;
		this.category = category;
		this.year = year;
	}

	//Getters et setters 
	
	public Double getTurnover() {
		return turnover;
	}

	public void setTurnover(Double turnover) {
		this.turnover = turnover;
	}

	public Double getOperatingProfit() {
		return operatingProfit;
	}

	public void setOperatingProfit(Double operatingProfit) {
		this.operatingProfit = operatingProfit;
	}

	public Double getNetProfit() {
		return netProfit;
	}

	public void setNetProfit(Double netProfit) {
		this.netProfit = netProfit;
	}

	public Double getReportedEPS() {
		return reportedEPS;
	}

	public void setReportedEPS(Double reportedEPS) {
		this.reportedEPS = reportedEPS;
	}

	public Double getCurrentAssets() {
		return currentAssets;
	}

	public void setCurrentAssets(Double currentAssets) {
		this.currentAssets = currentAssets;
	}

	public Double getNonCurrentAssets() {
		return nonCurrentAssets;
	}

	public void setNonCurrentAssets(Double nonCurrentAssets) {
		this.nonCurrentAssets = nonCurrentAssets;
	}

	public Double getTotalAssets() {
		return totalAssets;
	}

	public void setTotalAssets(Double totalAssets) {
		this.totalAssets = totalAssets;
	}

	public Double getCurrentLiabilities() {
		return currentLiabilities;
	}

	public void setCurrentLiabilities(Double currentLiabilities) {
		this.currentLiabilities = currentLiabilities;
	}

	public Double getTotalLiabilities() {
		return totalLiabilities;
	}

	public void setTotalLiabilities(Double totalLiabilities) {
		this.totalLiabilities = totalLiabilities;
	}

	public Double getTotalEquity() {
		return totalEquity;
	}

	public void setTotalEquity(Double totalEquity) {
		this.totalEquity = totalEquity;
	}

	public Double getOperatingCashFlow() {
		return operatingCashFlow;
	}

	public void setOperatingCashFlow(Double operatingCashFlow) {
		this.operatingCashFlow = operatingCashFlow;
	}

	public Double getNetChangeinCash() {
		return netChangeinCash;
	}

	public void setNetChangeinCash(Double netChangeinCash) {
		this.netChangeinCash = netChangeinCash;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((currentAssets == null) ? 0 : currentAssets.hashCode());
		result = prime * result + ((currentLiabilities == null) ? 0 : currentLiabilities.hashCode());
		result = prime * result + ((netChangeinCash == null) ? 0 : netChangeinCash.hashCode());
		result = prime * result + ((netProfit == null) ? 0 : netProfit.hashCode());
		result = prime * result + ((nonCurrentAssets == null) ? 0 : nonCurrentAssets.hashCode());
		result = prime * result + ((operatingCashFlow == null) ? 0 : operatingCashFlow.hashCode());
		result = prime * result + ((operatingProfit == null) ? 0 : operatingProfit.hashCode());
		result = prime * result + ((reportedEPS == null) ? 0 : reportedEPS.hashCode());
		result = prime * result + ((totalAssets == null) ? 0 : totalAssets.hashCode());
		result = prime * result + ((totalEquity == null) ? 0 : totalEquity.hashCode());
		result = prime * result + ((totalLiabilities == null) ? 0 : totalLiabilities.hashCode());
		result = prime * result + ((turnover == null) ? 0 : turnover.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Financial other = (Financial) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (currentAssets == null) {
			if (other.currentAssets != null)
				return false;
		} else if (!currentAssets.equals(other.currentAssets))
			return false;
		if (currentLiabilities == null) {
			if (other.currentLiabilities != null)
				return false;
		} else if (!currentLiabilities.equals(other.currentLiabilities))
			return false;
		if (netChangeinCash == null) {
			if (other.netChangeinCash != null)
				return false;
		} else if (!netChangeinCash.equals(other.netChangeinCash))
			return false;
		if (netProfit == null) {
			if (other.netProfit != null)
				return false;
		} else if (!netProfit.equals(other.netProfit))
			return false;
		if (nonCurrentAssets == null) {
			if (other.nonCurrentAssets != null)
				return false;
		} else if (!nonCurrentAssets.equals(other.nonCurrentAssets))
			return false;
		if (operatingCashFlow == null) {
			if (other.operatingCashFlow != null)
				return false;
		} else if (!operatingCashFlow.equals(other.operatingCashFlow))
			return false;
		if (operatingProfit == null) {
			if (other.operatingProfit != null)
				return false;
		} else if (!operatingProfit.equals(other.operatingProfit))
			return false;
		if (reportedEPS == null) {
			if (other.reportedEPS != null)
				return false;
		} else if (!reportedEPS.equals(other.reportedEPS))
			return false;
		if (totalAssets == null) {
			if (other.totalAssets != null)
				return false;
		} else if (!totalAssets.equals(other.totalAssets))
			return false;
		if (totalEquity == null) {
			if (other.totalEquity != null)
				return false;
		} else if (!totalEquity.equals(other.totalEquity))
			return false;
		if (totalLiabilities == null) {
			if (other.totalLiabilities != null)
				return false;
		} else if (!totalLiabilities.equals(other.totalLiabilities))
			return false;
		if (turnover == null) {
			if (other.turnover != null)
				return false;
		} else if (!turnover.equals(other.turnover))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Financial [turnover=" + turnover + ", operatingProfit=" + operatingProfit + ", netProfit=" + netProfit
				+ ", reportedEPS=" + reportedEPS + ", currentAssets=" + currentAssets + ", nonCurrentAssets="
				+ nonCurrentAssets + ", totalAssets=" + totalAssets + ", currentLiabilities=" + currentLiabilities
				+ ", totalLiabilities=" + totalLiabilities + ", totalEquity=" + totalEquity + ", operatingCashFlow="
				+ operatingCashFlow + ", netChangeinCash=" + netChangeinCash + ", category=" + category + ", year="
				+ year + "]";
	}
}