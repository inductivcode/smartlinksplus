package com.inductivtechnologies.smartlinksplusEjb.entities.market;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Company
 **/
@Entity(value = "company", noClassnameStored = true)
public class Company implements Serializable{
	
	public static final String COMPANY_DATA_NAME = "company";
	public static final String COMPANY_DATA_NAME_PLU = "companies";
	
	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = -7388236687444366542L;

	/**
	 * L'id de la compagnie
	 */
	@Id
	private ObjectId idCompany;
	
	/**
	 * Le nom de la compagnie
	 */
	private String name;

	/**
	 * Le symbole de la compagnie
	 */
	private String symbol;
	
	/**
	 * Le profile de la compagnie
	 */
	@Embedded
	private ProfileComp profile;
	
	/**
	 * Outlook de la compagnie
	 */
	@Embedded
	private OutLook outlook;
	
	/**
	 * Financial de la companie
	 */
	@Embedded
	private List<Financial> financial = new ArrayList<Financial>();
	
	/**
	 * Constructeur
	 */
	public Company() {
		super();
	}

	/**
	 * Constructeur
	*/
	public Company(ObjectId idCompany, String name, String symbol,
			ProfileComp profile, OutLook outlook,
			List<Financial> financial){
		
		super();
		this.idCompany = idCompany;
		this.name = name;
		this.symbol = symbol;
		this.profile = profile;
		this.outlook = outlook;
		this.financial = financial;
	}
	
	//Getters et setters 

	public List<Financial> getFinancial() {
		return financial;
	}

	public void setFinancial(List<Financial> financial) {
		this.financial = financial;
	}

	public ObjectId getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(ObjectId idCompany) {
		this.idCompany = idCompany;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public ProfileComp getProfile() {
		return profile;
	}

	public void setProfile(ProfileComp profile) {
		this.profile = profile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OutLook getOutlook() {
		return outlook;
	}

	public void setOutlook(OutLook outlook) {
		this.outlook = outlook;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((financial == null) ? 0 : financial.hashCode());
		result = prime * result + ((idCompany == null) ? 0 : idCompany.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((outlook == null) ? 0 : outlook.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (financial == null) {
			if (other.financial != null)
				return false;
		} else if (!financial.equals(other.financial))
			return false;
		if (idCompany == null) {
			if (other.idCompany != null)
				return false;
		} else if (!idCompany.equals(other.idCompany))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (outlook == null) {
			if (other.outlook != null)
				return false;
		} else if (!outlook.equals(other.outlook))
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Company [idCompany=" + idCompany + ", name=" + name + ", symbol=" + symbol + ", profile=" + profile
				+ ", outlook=" + outlook + ", financial=" + financial + "]";
	}
}