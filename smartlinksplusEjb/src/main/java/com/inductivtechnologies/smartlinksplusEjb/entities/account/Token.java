package com.inductivtechnologies.smartlinksplusEjb.entities.account;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import com.inductivtechnologies.smartlinksplusEjb.utils.DateUtils;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;

/**
 * Token : Pour la gestion de la sécurité 
 **/

@Entity(value="tokens", noClassnameStored = true)
public class Token implements Serializable {
	
	public static final int TOKEN_LENGHT = 34;
	
	public static final String TOKEN_DATA_NAME = "token";

	/**
	 * serialVersionUID Pour la sérialisation 
	 */
	private static final long serialVersionUID = -6561161271047054864L;

	/**
	 * idToken, identifiant unique de l'objet
	 */
	@Id
	private ObjectId idToken;
	
	/**
	 * value, La valeur du token 
	 */
	private String value;
	
	/**
	 * expirationDate : Expiration du token (Après 24H)
	 */
	private Date expirationDate;
	
	/**
	 * creationDate : La date de création du Token
	 */
	private Date creationDate;
	
	/**
	 * Constructeur 
	 */
	public Token() {
		super();
		this.value = Util.generateSecureToken(TOKEN_LENGHT);
		this.creationDate = new Date();
		
		DateTime now = DateTime.now();
		this.expirationDate = DateUtils.convertDateTimeToDate(now.plusHours(12));
	}
	
	// Getters et setters
	public ObjectId getIdToken() {
		return idToken;
	}

	public void setIdToken(ObjectId idToken) {
		this.idToken = idToken;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}	
}
