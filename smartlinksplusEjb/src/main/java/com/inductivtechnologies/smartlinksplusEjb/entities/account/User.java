package com.inductivtechnologies.smartlinksplusEjb.entities.account;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

@Entity(value = "user", noClassnameStored = false)
public class User implements Serializable{

	/**
	 * serialVersionUID identifiant unique pour 
	 * la sérialisation de l'objet, permettant la persistance 
	 * de l'objet.
	 */
	private static final long serialVersionUID = -1372476242874736935L;

	/**
	 * idUser identifiant unique de l'objet, modifiable
	 */
	@Id
	private ObjectId idUser;
	
	/**
	 * credential : Informationsd'authentification
	 */
	@Embedded
	private Credential credential;
	
	/**
	 * token : Le token 
	 */
	@Reference
	private Token token;
	
	/**
	 * Constructeur
	*/
	public User(){
		super();
	}
	
	// Getters and setters
	
	public void setIdUser(ObjectId idUser) {
		this.idUser = idUser;
	}

	public void setCredentials(Credential credential) {
		this.credential = credential;
	}

	public Credential getCredentials() {
		return credential;
	}

	public ObjectId getIdUser() {
		return idUser;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}
}

