package com.inductivtechnologies.smartlinksplusEjb.entities.account;

import java.io.Serializable;

/**
 * Credential : Pour faire l'authenfification
*/
public class Credential implements Serializable {

	/**
	 * Pour la sérialisation 
	*/
	private static final long serialVersionUID = 1722601011268145207L;

	/**
	 * Nom utilisateur 
	*/
	private String username;

	/**
	 * Mot de passe 
	*/
	private String password;
	
	/**
	 * Constructeur
	*/
	public Credential() {
		super();
	}

	/**
	 * Constructeur
	*/
	public Credential(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	// Getters and setters
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
