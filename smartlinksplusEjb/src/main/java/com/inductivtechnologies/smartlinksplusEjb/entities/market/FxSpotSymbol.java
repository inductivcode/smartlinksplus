package com.inductivtechnologies.smartlinksplusEjb.entities.market;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * FxSpotSymbol : Englobe les données relatives aux symboles des Fx Spot 
 */
@Entity(value = "fxSpotSymbol", noClassnameStored = true)
public class FxSpotSymbol implements Serializable{
	
	public static final String FXSPOTSYMBOL_DATA_NAME = "fxspot_symbols";
	public static final String FXSPOTSYMBOL_DATA_NAME_SIN = "fxspot_symbol";

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = -4778383311970533807L;
	
	/**
	 * La clé pimaire
	 */
	@Id
	private ObjectId fxSpotSymbolId;
	
	/**
	 * La valeur du symbole Ex EURUSD
	 */
	private String symbolValue;
	
	/**
	 * La description du symbole Ex pour EURUSD : Euro US Dollar
	 */
	private String symbolDescription;
	
	/**
	 * Date de création
	 */
	private final Date creationDate;
	
	/**
	 * Constructeur
	 */
	public FxSpotSymbol(){
		super();
		this.creationDate = new Date();
	}

	public ObjectId getFxSpotSymbolId() {
		return fxSpotSymbolId;
	}

	public void setFxSpotSymbolId(ObjectId fxSpotSymbolId) {
		this.fxSpotSymbolId = fxSpotSymbolId;
	}

	public String getSymbolValue() {
		return symbolValue;
	}

	public void setSymbolValue(String symbolValue) {
		this.symbolValue = symbolValue;
	}

	public String getSymbolDescription() {
		return symbolDescription;
	}

	public void setSymbolDescription(String symbolDescription) {
		this.symbolDescription = symbolDescription;
	}

	public Date getCreationDate() {
		return creationDate;
	}
}
