package com.inductivtechnologies.smartlinksplusEjb.entities.market;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * RawMarketData : Englobe les données d'un marché 
 */
@Entity(value = "rawMarketData", noClassnameStored = true)
public class RawMarketData implements Serializable {
	
	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = 3430717873370995305L;

	/**
	 * L'id du RawMarketData courant
	 */
	@Id
	private ObjectId idRawMarketData;
	
	/**
	 * Le symbole de l'entreprise
	 */
	private String symbol;
	
	/**
	 * Prix du produit financier à l'ouverture du marché financier
	 */
	private Double openedValue;
	
	/**
	 * Le prix d'un produit financier à la fermeture d'un marché financier
	 */
	private Double closedValue;

	/**
	 * Le prix le plus bas d'un produit financier dans une journée
	 */
	private Double lowestValue;

	/**
	 * Le prix le plus haut d'un produit financier dans une journée
	 */
	private Double highestValue;

	/**
	 * Le dernier prix d'un produit financier sur un marché financier
	 */
	private Double lastValue;
	
	/**
	 *  Le nombre d'actions qui ont été négociées, achetées ou vendues tout au long de la journée
	 */
	private Double volume;

	/**
	 * Prix que les gens sont prêts à payer pour avoir le produit financier
	 */
	private Double askedValue;
	
	/**
	 * Prix que les gens sont prêts à payer pour avoir le produit financier
	 */
	private Double adjclose;
	
	/**
	 * changeValue en pourcentage 
	 */
	private String changeValue;
	
	/**
	 * La date à laquelle les prix sont récupérés
	 */
	private Date retrievedDate;
	
	/**
	 * Le type du marché
	 */
	private String type;
	
	/**
	 * Date de création du RawMarketData
	 */
	private final Date creationDate;

	/**
	 * Constructeur
	 */
	public RawMarketData() {
		super();
		creationDate = new Date();
	}

	public ObjectId getIdRawMarketData() {
		return idRawMarketData;
	}

	public void setIdRawMarketData(ObjectId idRawMarketData) {
		this.idRawMarketData = idRawMarketData;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getOpenedValue() {
		return openedValue;
	}

	public void setOpenedValue(Double openedValue) {
		this.openedValue = openedValue;
	}

	public Double getClosedValue() {
		return closedValue;
	}

	public void setClosedValue(Double closedValue) {
		this.closedValue = closedValue;
	}

	public Double getLowestValue() {
		return lowestValue;
	}

	public void setLowestValue(Double lowestValue) {
		this.lowestValue = lowestValue;
	}

	public Double getHighestValue() {
		return highestValue;
	}

	public void setHighestValue(Double highestValue) {
		this.highestValue = highestValue;
	}

	public Double getLastValue() {
		return lastValue;
	}

	public void setLastValue(Double lastValue) {
		this.lastValue = lastValue;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getAskedValue() {
		return askedValue;
	}

	public void setAskedValue(Double askedValue) {
		this.askedValue = askedValue;
	}

	public Double getAdjclose() {
		return adjclose;
	}

	public void setAdjclose(Double adjclose) {
		this.adjclose = adjclose;
	}

	public String getChangeValue() {
		return changeValue;
	}

	public void setChangeValue(String changeValue) {
		this.changeValue = changeValue;
	}

	public Date getRetrievedDate() {
		return retrievedDate;
	}

	public void setRetrievedDate(Date retrievedDate) {
		this.retrievedDate = retrievedDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreationDate() {
		return creationDate;
	}
}
