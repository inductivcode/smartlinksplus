package com.inductivtechnologies.smartlinksplusEjb.entities.market;

import java.io.Serializable;

import org.mongodb.morphia.annotations.Embedded;

/**
 * OutLook : outlook de la compagnie
 **/
@Embedded
public class OutLook implements Serializable{

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = 471967668680782826L;
	
	/**
	 * Evènement récent 
	 */
	private String event;
	
	/**
	 * Evènement suivant 
	 */
	private String nextEvent;

	/**
	 * Constructeur
	 */
	public OutLook() {
		super();
	}

	/**
	 * Constructeur
	 */
	public OutLook(String event, String nextEvent) {
		super();
		this.event = event;
		this.nextEvent = nextEvent;
	}

	public String getEvent() {
		return event;
	}
	
	public void setEvent(String event) {
		this.event = event;
	}

	public String getNextEvent() {
		return nextEvent;
	}

	public void setNextEvent(String nextEvent) {
		this.nextEvent = nextEvent;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((event == null) ? 0 : event.hashCode());
		result = prime * result + ((nextEvent == null) ? 0 : nextEvent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OutLook other = (OutLook) obj;
		if (event == null) {
			if (other.event != null)
				return false;
		} else if (!event.equals(other.event))
			return false;
		if (nextEvent == null) {
			if (other.nextEvent != null)
				return false;
		} else if (!nextEvent.equals(other.nextEvent))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OutLook [event=" + event + ", nextEvent=" + nextEvent + "]";
	}
}
