package com.inductivtechnologies.smartlinksplusEjb.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;

import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.readers.CSVReader;

/**
 * Util : Regroupe quelques ùéthodes utiles
 */
public class Util {
	
	/**
     * Fournit les symboles de type EQUITIES contenu dans un fichier CSV
     *  @param csvFileName Le fichier à lire
     * @return La liste des symboles
     */
	public static List<String> allEquitiesSymbolsFromCSVFile(String csvFileName){
		 return CSVReader.allEquitiesSymbols(csvFileName);
	}
	
	/**
     * Fournit les symboles de type Fx Spot contenu dans un fichier CSV
     * @param csvFileName Le fichier à lire
     * @return La liste des symboles
     */
	public static List<FxSpotSymbol> allFxSpotSymbolsFromCSVFile(String csvFileName){
		return CSVReader.allFXSpotSymbols(csvFileName);
	}
	
	/**
	 * Construit un objet RawMarketData de type Equities à partir d'une ligne du fichier CSV
	 * @param columns - Les colonnes correspondant à une ligne du fichier csv
	 * @return RawMarketData> : Le marché construit  
	 */
	public static RawMarketData buildRawMarketDataEquities(String[] columns){
		RawMarketData marketData = null;
		
		if(!Util.containNullValue(columns)){
			Date retrievedDate = DateUtils.parseJavaDate(columns[0], Constants.DATA_FORMAT_DATE);
			
			Double openedValue = Double.valueOf(columns[1]);
			Double highestValue = Double.valueOf(columns[2]);
			Double lowestValue = Double.valueOf(columns[3]);
			Double closedValue = Double.valueOf(columns[4]);
			Double adjclose = Double.valueOf(columns[5]);
			Double volume = Double.valueOf(columns[6]);
			
			marketData = new RawMarketData();
			marketData.setRetrievedDate(retrievedDate);
			marketData.setOpenedValue(openedValue);
			marketData.setHighestValue(highestValue);
			marketData.setLowestValue(lowestValue);
			marketData.setClosedValue(closedValue);
			marketData.setAdjclose(adjclose);
			marketData.setVolume(volume);
		}
		
		return marketData;
	}
	
	/**
	 * Construit un objet RawMarketData de type FxSpot à partir d'une ligne du fichier CSV
	 * @param columns - Les colonnes correspondant à une ligne du fichier csv
	 * @return RawMarketData> : Le marché construit  
	 */
	public static RawMarketData buildRawMarketDataFxSpot(String[] columns){
		RawMarketData marketData = null;
		
		if(!Util.containNullValue(columns)){
			if(!columns[0].isEmpty()){
				Date retrievedDate = DateUtils.parseInvestingComDate(columns[0]);
				
				Double openedValue = Double.valueOf(columns[2]);
				Double highestValue = Double.valueOf(columns[3]);
				Double lowestValue = Double.valueOf(columns[4]);
				Double closedValue = Double.valueOf(columns[1]);
				String changeValue = columns[5];
				
				marketData = new RawMarketData();
				marketData.setRetrievedDate(retrievedDate);
				marketData.setOpenedValue(openedValue);
				marketData.setHighestValue(highestValue);
				marketData.setLowestValue(lowestValue);
				marketData.setClosedValue(closedValue);
				marketData.setChangeValue(changeValue);
			}
		}
		
		return marketData;
	}
	
	/**
	 * Construit un objet buildFxSpotSymbol à partir d'une ligne du fichier CSV
	 * @param columns - Les colonnes correspondant à une ligne du fichier csv
	 * @return FxSpotSymbol> : Le symbole construit  
	 */
	public static FxSpotSymbol buildFxSpotSymbol(String[] columns){
		FxSpotSymbol fxSpotSymbol = null;
		
		if(!Util.containNullValue(columns)){
			fxSpotSymbol = new FxSpotSymbol();
			
			fxSpotSymbol.setSymbolValue(columns[0]);
			fxSpotSymbol.setSymbolDescription(columns[1]);
		}
		
		return fxSpotSymbol;
	}

	/**
	 * Construit une chaine de caractères à partir d'un 
	 * marché afin de l'inserer dans un fichier csv Ex : 12-03-2018, 0.345, 0.345, 0.345 ...
	 * @param type Le type du marché EQUITIES OU FXSPOT
	 * @param market La donnée à transformer
	 * @return String  
	 */
	public static String transformRawMarketDataToString(String type, RawMarketData market){
		String lineValues  = "";
		
		switch(type){
			case Constants.EQUITY_TYPE : 
				lineValues = DateUtils.printJavaDate(market.getRetrievedDate(), Constants.PRINT_DATE_FORMAT) + "," + 
						String.valueOf(market.getOpenedValue()) + "," + 
						String.valueOf(market.getHighestValue()) + "," +
						String.valueOf(market.getLowestValue()) + "," +
						String.valueOf(market.getClosedValue()) + "," +
						String.valueOf(market.getAdjclose()) + "," +
						String.valueOf(market.getVolume());
				break;
			case Constants.FXSPOT_TYPE : 
				lineValues = DateUtils.printJavaDate(market.getRetrievedDate(), Constants.PRINT_DATE_FORMAT) + "," + 
						String.valueOf(market.getClosedValue()) + "," + 
						String.valueOf(market.getOpenedValue()) + "," +
						String.valueOf(market.getHighestValue()) + "," +
						String.valueOf(market.getLowestValue()) + "," +
						market.getChangeValue();
				break;
		}
		
		return lineValues;
	}
	
	/**
     * Retourne le chemin vers les fichiers csv des equities
     * @param symbol 
     * @return Le chemin correspondant
     */
	public static String csvFilePathEquities(String symbol){
		 String filename = String.format(Constants.CSV_FILE_NAME_EQUITIES, symbol);
	     return String.format(Constants.CSV_FILE_PATH_EQUITIES, filename) ;
	}
	
	/**
     * Retourne le chemin vers les fichiers csv des fx spot
     * @param symbol 
     * @return Le chemin correspondant
     */
	public static String csvFilePathFxSpot(String symbol){
		 String filename = String.format(Constants.CSV_FILE_NAME_FXSPOT,
				 symbol.substring(0,3), symbol.substring(3));
	     return String.format(Constants.CSV_FILE_PATH_FXSPOT, filename) ;
	}

	/**
     * Vérifie si le tableau de chaine de caractères contient une valeur  null
     * @param strings Tableau de chaines de caractères
     * @return Vrai si la liste contient "null" Faux sinon
     */
	public static boolean containNullValue(String[] strings){
		for(int i = 0; i < strings.length; i++){
			if(strings[i].equals("null")) return true;
		}
			
		return false;
	}
	
	/**
     * Met le premier caractère en majuscule
     * @param string
     * @return String
     */
    public static String toUpperFirstChar(String string){
        if(string.equals("")) return "";
        return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
    }
	
	/**
     * Retourne la position dune chaine dans un tableau
     * @param source Chaine source 
     * @param search Elément à chercher
     * @return Position de la chaine
     */
    public static int positionOfStringInArray(String[] source, String search){
        for(int i = 0; i < source.length; i++){
            if(source[i].equalsIgnoreCase(search)) return i;
        }
        return -1;
    }
	
	/**
     * Converti une liste de string en tableau 
     * @param list La liste 
     * @return Le tableau correspondant 
     */
	public static String[] listToArray(List<String> list){
		String[] array = new String[list.size()];
		for(int i = 0; i < list.size(); i++) array[i] = list.get(i);
		
		return array;
	}
	
	/**
     * Donné l'accès à un fichier s'il existe sinon le crée 
     * @param absolutePathFile Le chemin absolu du fichier  
     * @return boolean Vrai si tout s'est bien passe Faux sinon
     */
	public static boolean createFileIfNotExist(String absolutePathFile){
		boolean createFileResult = true;
		
		//On récupère le fichier s'il existe sinon on le crée
		File file = new File(absolutePathFile);
		
		if(!file.exists()){
			try {
				createFileResult = file.createNewFile();
			}catch (IOException exception) {
				createFileResult = false;
				System.out.println("IOException fichier non créé");
				System.out.println(exception);
			}
		}
		
		return createFileResult;
	}
	
	/**
     * Affiche un marché
     * @param market Le marché à afficher
     */
	public static void viewMarket(RawMarketData market){
		System.out.print("Date : " +
				DateUtils.printJavaDate(market.getRetrievedDate(), 
								Constants.PRINT_DATE_FORMAT));
		System.out.print(" / Type : " + market.getType());
		System.out.print(" / Open : " + market.getOpenedValue());
		
		System.out.println("");
	}
	
	/**
     * Affiche quelques marchés
     * @param markets Les marchés à afficher
     */
	public static void viewMarkets(List<RawMarketData> markets){
		if(!markets.isEmpty()){
			for(RawMarketData market : markets){
				viewMarket(market);
			}
		}else{
			System.out.println("Aucune données");
		}
	}
	
	/**
     * Affiche plusieurs fx spot symbols
     * @param fxSpotSymbols Les symboles à afficher
     */
	public static void viewFxSpotSymbols(List<FxSpotSymbol> fxSpotSymbols){
		if(!fxSpotSymbols.isEmpty()){
			for(FxSpotSymbol fxSpotSymbol : fxSpotSymbols){
				viewFxSpotSymbol(fxSpotSymbol);
			}
		}else{
			System.out.println("Aucune données");
		}
	}
	
	/**
     * Affiche un fx spot symbol 
     * @param fxSpotSymbol Le symbol à afficher
     */
	public static void viewFxSpotSymbol(FxSpotSymbol fxSpotSymbol){
		System.out.print(" Id : " + fxSpotSymbol.getFxSpotSymbolId().toHexString());
		System.out.print(" Symbole : " + fxSpotSymbol.getSymbolValue());
		System.out.print(" / Description : " + fxSpotSymbol.getSymbolDescription());
		System.out.print(" / Date de création : " +
				DateUtils.printJavaDate(fxSpotSymbol.getCreationDate(), 
								Constants.PRINT_DATE_FORMAT));
		
		System.out.println("");
	}
	
	/**
     * Génère un token sécurisé
     * @param lenght La taille du token 
     * @return Le token obtenu 
     */
	public static String generateSecureToken(int lenght){
		SecureRandom secureRandom = new SecureRandom();
		byte[] token = new byte[lenght];
		secureRandom.nextBytes(token);
		return new BigInteger(1, token).toString(16);
	}
	
	/**
     * Indique si un token a expiré ou pas
     *  @param token Le token 
     * @return Vrai si oui Faux sinon
     */
	public static boolean tokenIsAlwaysValid(Token token){
		if(System.currentTimeMillis() <= token.getExpirationDate().getTime()) return true;
		return false;
	}
}
