package com.inductivtechnologies.smartlinksplusEjb.utils;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * DateUtils : Regroupe quelques méthodes utiles pour les dates
 */
public class DateUtils {
	
	/**
     * Imprime une date dans un format donné 
     * @param date(Date) La date à imprimer
     * @param format Le format choisi
     * @return String La date au format voulu 
     */
	public static String printJavaDate(Date date, String format){
		return DateTimeFormat.forPattern(format).print(new DateTime(date.getTime()));
	}
	
	/**
     * Imprime une date dans un format donné 
     * @param date(DateTime) La date à imprimer
     * @param format Le format choisi
     * @return String La date au format voulu 
     */
	public static String printJodaTimeDate(DateTime date, String format){
		return DateTimeFormat.forPattern(format).print(date);
	}
	
	/**
     * Converti une chaine de caractères en date
     * @param date La valeur à convertir
     * @param format Le format de la date
     * @return Date La date voulu
     */
	public static Date parseJavaDate(String date, String format){
		DateTime dateTime = DateTimeFormat.forPattern(format).parseDateTime(date);
		
		return new Date(dateTime.getMillis());
	}
	
	/**
     * Converti une chaine de caractères en date
     * @param date La valeur à convertir
     * @param format Le format de la date
     * @return DateTime La date voulu
     */
	public static DateTime parseJodaTimeDate(String date, String format){
		return DateTimeFormat.forPattern(format).parseDateTime(date);
	}

	/**
     * Permet de convertir une Date(java.util.Date) en Joda DateTime(org.joda.time.DateTime)
     * @param date La valeur à convertir
     * @return La date obtenue
     */
	public static Date convertDateTimeToDate(DateTime date){
		return new Date(date.getMillis());
	}
	
	/**
     * Parse les dates des données récupérées sur investing.com (Ex Mar 12, 2014)
     * @param date La valeur à convertir
     * @return Date La date voulu
     */
	public static Date parseInvestingComDate(String date){
		String strDate = "%s-%s-%s";
		
		String strYear = date.substring(8);
		String strDay = date.substring(4, 6);
		
		int position = Util.positionOfStringInArray(Constants.MONTH_IN_ENGLISH, date.substring(0, 3));
		int month = position == -1 ? 1 : (position + 1);
		String strMonth = "";
		
		if(month < 10){
			strMonth = "0" + String.valueOf(month);
		}else{
			strMonth = String.valueOf(month);
		}
		
		return parseJavaDate(String.format(strDate, strYear, strMonth, strDay),Constants.DATA_FORMAT_DATE);
	}
	
	/**
     * Permet de constrire les date utilisé par dans les requêtes Yahoo finance
     * @param date La valeur à convertir
     * @return logn La date en millisecondes près
     */
	public static long yahooPeriod(DateTime date){
		return date.getMillis() / 1000;
	}
	
	/**
	 * Valide une date 
	 * @param strDate La date à valider au format String 
	 * @return Vrai si la date correspond au format souhaité Faux sinon 
	*/
	public static boolean isValidDate(String strDate){
	    try{
	        DateTimeFormatter formatDate = DateTimeFormat.forPattern(Constants.PRINT_DATE_FORMAT);
	        formatDate.parseDateTime(strDate);
	    }catch (Exception e){
	        return false;
	    }
	    return true;
	}

}
