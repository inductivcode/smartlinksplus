package com.inductivtechnologies.smartlinksplusEjb.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.inductivtechnologies.smartlinksplusEjb.utils.Constants;
 
/**
 * Charge des contenus HTML
*/
public class HTTPLoader {
	
	/**
	 * Open a url and read text data, for example html file
	 * @param _url Url
	 * @return String file content
	*/
	public static void getTextFile(String _url, String name) {
		BufferedReader reader = null;

		try{
			URL url = new URL(_url);
			URLConnection urlConnection = url.openConnection();
			
			reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			String namef = Constants.HTML_FILE_PATH + name + ".html";
			
			File file = new File(namef);
			FileWriter fr = new FileWriter(file);
			
			while ((line = reader.readLine()) != null) {
				fr.write(line);
				fr.append("\n");
			}
			
			fr.close();
		}catch(IOException ex) {
			Logger.getLogger(HTTPLoader.class.getName()).log(Level.SEVERE, null, ex);
		}finally{
			try{
				if (reader != null) {
					reader.close();
				}
			}catch(IOException ex) {
				Logger.getLogger(HTTPLoader.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
	/**
	 * saveCompanyProfile
	 * @param _url Url
	 * @param name
	*/
	public static void saveCompanyProfile(String _url, String name) {
		BufferedReader reader = null;

		try {
			URL url = new URL(_url);
			URLConnection urlConnection = url.openConnection();
			
			reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			String namef = Constants.COMPANIES_PROFILES_FILE_PATH + name + ".html";
			
			File file = new File(namef);
			FileWriter fr = new FileWriter(file);
			while ((line = reader.readLine()) != null) {
				fr.write(line);
				fr.append("\n");
			}
			
			fr.close();
		}catch(IOException ex) {
			Logger.getLogger(HTTPLoader.class.getName()).log(Level.SEVERE, null, ex);
		}finally{
			try {
				if (reader != null) {
					reader.close();
				}
			}catch(IOException ex) {
				Logger.getLogger(HTTPLoader.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	/**
	 * fonction qui prend en entrée un flux contenant du code html et sauvegarde
	 * le contenu dans un string
	 * @param file
	 * @return Laine correspondante
	*/
	public static String transformFileToString(File file) {
		BufferedReader reader = null;
		String html = "";

		try{
			FileReader fr = new FileReader(file);
			reader = new BufferedReader(fr);
			String line = null;

			while((line = reader.readLine()) != null) {
				html = html.concat(line).concat("\n");
			}
			
			fr.close();
			reader.close();
		}catch(IOException ex) {
			Logger.getLogger(HTTPLoader.class.getName()).log(Level.SEVERE, null, ex);
		}finally{
			try{
				if (reader != null) {
					reader.close();
				}
			}catch(IOException ex) {
				Logger.getLogger(HTTPLoader.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		
		return html;
	}
    
} 