package com.inductivtechnologies.smartlinksplusEjb.utils;

/**
 * Constants : Regroupe quelques constantes
 */
public class Constants {

	public static final String CSV_FILE_NAME_EQUITIES = "%s_Equities.csv";
	
	public static final String CSV_FILE_NAME_FXSPOT = "%s_%s Historical Data.csv";
	
	public static final String CRUMB_LABEL = "CrumbStore";
	
	public static final String WILDFLY_HOME = "/opt/wildfly-10.1.0.Final";
	
	public static final String CSV_FILE_PATH_EQUITIES = WILDFLY_HOME + "/ressources/csvData/Equities/%s";
	
	public static final String CSV_FILE_PATH_FXSPOT = WILDFLY_HOME + "/ressources/csvData/FxSpot/%s";
	
	public static final String CSV_FILE_PATH_EQUITIES_SYMBOLS = WILDFLY_HOME + "/ressources/csvData/FTSE_100Companies.csv";
	
	public static final String CSV_FILE_PATH_FXSPOT_SYMBOLS = WILDFLY_HOME + "/ressources/csvData/FXSpot_symbols.csv";
	
	public static final String FXSPOT_HTML_FILE_PATH = WILDFLY_HOME + "/ressources/htmlData/lastFxSpot.html";
	
	public static final String DOWNLOAD_FILE_PATH = WILDFLY_HOME + "/ressources/downloadData/%s_Historical_data.csv";
	
	public static final String HTML_FILE_PATH = WILDFLY_HOME + "/ressources/htmlData/";
	
	public static final String[] MONTH_IN_ENGLISH = { "Jan", "Feb", "Mar", "Apr", 
	                                              "May", "Jun", "Jul", "Aug", "Sep",
	                                              "Oct", "Nov", "Dec"
	                                            };
	
	//Les colonnes du fichier csv contenant les FXSPOT
	public static final String[] FXSPOT_CSV_COLUMNS = { "Date", "Price", "Open", "High", "Low", "change %"};
	
	//On se sert de ce format pour enregistrer les date dans la base de données
	public static final String DATA_FORMAT_DATE = "YYYY-MM-dd";
	
	// Pour afficher la date des données 
	public static final String PRINT_DATE_FORMAT = "dd-MM-YYYY";
	
	// Pour afficher les messages lors de la mise à jour des données 
	public static final String US_DATETIME_FORMAT = "YYYY-MM-dd HH:MM:SS";
	
	public static final String EQUITY_TYPE = "EQUITY";
	
	public static final String FXSPOT_TYPE = "FX_SPOT";
	
	public static final String COMPANIES_PROFILES_FILE_PATH = WILDFLY_HOME + "/ressources/htmlData/companiesProfiles/";
}
