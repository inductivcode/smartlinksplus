package com.inductivtechnologies.smartlinksplusEjb.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.inductivtechnologies.smartlinksplusEjb.entities.market.Company;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.Financial;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.OutLook;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.ProfileComp;
import com.inductivtechnologies.smartlinksplusEjb.remotedata.writers.RemoteDataWriter;

/**
 * Récupère des données dans des fichiers HTML
*/
public class HtmlScrapper {

	public static Map<String, String> complink = new HashMap<>();

	public static final String SECTOR = "Sector";

	public static final String INDUSTRY = "Industry";

	public static final String STOCKSTYLE = "Stock Style";

	public static final String FISCALYEARENDS = "Fiscal Year Ends";

	public static final String EMPLOYEES = "Employees";

	public static final String DESCRIPTION = "Description";

	private static final String TURNOVER = "turnover";

	private static final String OPERATINGPROFIT = "operating profit";

	private static final String NETPROFIT = "net profit";

	private static final String REPORTEDEPS = "reported eps";

	private static final String CURRENTASSETS = "current assets";

	private static final String NONCURRENTASSETS = "non current assets";

	private static final String TOTALASSETS = "total assets";

	private static final String CURRENTLIABILITIES = "current liabilities";

	private static final String TOTALLIABILITIES = "total liabilities";

	private static final String TOTALEQUITY = "total equity";

	private static final String OPERATINGCASHFLOW = "operating cash dlow";
	
	//Id de la  balise <table> qui affiche les fxspot 
	private static final String DATA_FXSPOT_TABLE_ID = "curr_table";
	
	private static final String NETCHANGEINCASH = "net change in cash";

	public HtmlScrapper() {
		super();
	}
	
	/**
	 * Fonction qui charge la liste des compagnies ainsi que le lien vers leurs
	 * informations et le sauvegarde dans un fichier CSV
	*/
	public static void loadCompanies() throws IOException {
		String content = "";

		for (int i = 1; i < 8; i++) {
			File file = new File(Constants.HTML_FILE_PATH + "compagnies" + i + ".html");
			Document doc = Jsoup.parse(file, "UTF-8");

			if (null != doc) {
				Elements elements = doc.getElementsByAttribute("title");

				for (Element link : elements) {
					String linkHref = link.attr("href");
					String linkTitle = link.attr("title");

					if ((null != linkHref && !linkHref.isEmpty()) && (null != linkTitle && !linkTitle.isEmpty())) {
						complink.put(linkTitle, linkHref);
						content = content + linkTitle + "," + linkHref + "\n";
					}
				}
			}
		}
		
		RemoteDataWriter.writeIntoAFile("companiesLinks.csv", content);
	}

	/**
	 * Fonction qui fait la recherche des informations sur les compagnies et les
	 * stockent dans un fichier CSV
	 * @throws IOException
	*/
	public static List<Company> loadCompagniesInfo() throws IOException, NullPointerException {
		List<Company> companies = new ArrayList<Company>();
		
		Map<String, String> comprofile = null;
		Map<String, String> compoutlook = null ;
		Map<String, String> compfinancial = null;

		ProfileComp profile ;
		OutLook outlook;
		
		List<Financial> financiales = new ArrayList<>();
		
        List<String> companiesName = companiesName();
        
		for (String nameC : companiesName) {
			File file = new File(Constants.COMPANIES_PROFILES_FILE_PATH + nameC + ".html");

			if (file.exists()) {
				comprofile = loadCompanyProfileInfo(nameC, file);
				compoutlook = loadCompanyOutlookInfo(nameC, file);
				compfinancial = loadCompanyFinancialInfo(nameC, file);
				
                profile = new ProfileComp();
                
				Set<String> profileSet = comprofile.keySet();
				
				for (String st : profileSet) {
					String value = comprofile.get(st);
					switch (st) {
					case SECTOR:
						profile.setSector(value);
						break;
					case INDUSTRY:
						profile.setIndustry(value);
						break;
					case STOCKSTYLE:
						profile.setStockStyle(value);
						break;
					case EMPLOYEES:
						profile.setNbEmp(transformStringToDouble(value));
						break;
					case FISCALYEARENDS:
						profile.setFiscalYearEnds(value);
						break;
					case DESCRIPTION:
						profile.setDescription(value);
						break;
					}
				}
				
				Set<String> outlookSet = compoutlook.keySet();
                outlook = new OutLook();
				for (String st : outlookSet) {
					switch (st) {
					case "event":
						outlook.setEvent(compoutlook.get(st));
						break;
					case "nextEvent":
						outlook.setNextEvent(compoutlook.get(st));
						break;
					}
				}

				Set<String> financeSet = compfinancial.keySet();
              
				financiales = fillFinancials( financeSet,compfinancial);
//				System.out.println("taille 2 de financials >>>"+financiales.size()+">>>>>>contenu>>>>"+financiales);
				String symbol = getCompanySymbol(nameC, file);
				Company company = new Company(null, nameC, symbol, profile, outlook, financiales);
//				System.out.println("company >>>"+company.getFinancial());
				companies.add(company);
			}
			
			
            compfinancial.clear();;
            compoutlook.clear();
            comprofile.clear();
		}
		
        return companies;
	}
	
	/**
	 * fillFinancials
	*/
	public static List<Financial> fillFinancials(Set<String> financeSet, Map<String,String> compfinancial){
		List<Financial> financials = new ArrayList<>();
		 Set<String> years = new HashSet<String>();
		 
		 for(String st : financeSet) {
			String[] str = st.split("_");
			if (str.length == 3) {
					years.add(str[1]);
			}
		 }
		    
//		 System.out.println("years of :"+years);
		 for(String yd : years){
			 Financial financial = new Financial();
		
		     for(String st : financeSet) {
				String[] str = st.split("_");
	
				String year = "";
				String category = "";
				String tex = "";
					
				if(str.length == 3) {
					year = str[1];
					if(year.equalsIgnoreCase(yd)){  
						category = str[2];
						tex = str[0];
						String value = compfinancial.get(st);
							
						switch(tex) {
							case TURNOVER:
								financial.setTurnover(transformStringToDouble(value));
								break;
							case OPERATINGPROFIT:
								financial.setOperatingProfit(transformStringToDouble(value));
								break;
							case NETPROFIT:
								financial.setNetProfit(transformStringToDouble(value));
								break;
							case REPORTEDEPS:
								financial.setReportedEPS(transformStringToDouble(value));
								break;
							case CURRENTASSETS:
								financial.setCurrentAssets(transformStringToDouble(value));
								break;
							case NONCURRENTASSETS:
								financial.setNonCurrentAssets(transformStringToDouble(value));
								break;
							case TOTALASSETS:
								financial.setTotalAssets(transformStringToDouble(value));
								break;
							case CURRENTLIABILITIES:
								financial.setCurrentLiabilities(transformStringToDouble(value));
								break;
							case TOTALLIABILITIES:
								financial.setTotalLiabilities(transformStringToDouble(value));
								break;
							case TOTALEQUITY:
								financial.setTotalEquity(transformStringToDouble(value));
								break;
							case OPERATINGCASHFLOW:
								financial.setOperatingCashFlow(transformStringToDouble(value));
								break;
							case NETCHANGEINCASH:
								financial.setNetChangeinCash(transformStringToDouble(value));
								break;
							}
							financial.setCategory(category);
							financial.setYear(year);
					     }
					}
			}
			financials.add(financial);
		}
		 
	    return financials;
	}
	
	/**
	 * loadCompanyProfileInfo
	*/
	public static Map<String, String> loadCompanyProfileInfo(String nameComp, File file)
			throws IOException, NullPointerException {

		String sector = "", industry = "", stocktyle = "", fiscalYearEnds = "", employees = "", description = "";
		Map<String, String> profileComp = new HashMap<>();

		Document doc = Jsoup.parse(file, "UTF-8");
		Element element = doc.getElementById("CompanyProfile");

		if(null != element) {
			Elements elements = element.children();

			for (Element elmt : elements) {
				String descrip = "";

				if (elmt.hasAttr("class") & elmt.attr("class").equalsIgnoreCase("item")) {
					descrip = elmt.text();
					int nbchild = elmt.children().size();

					if(nbchild == 0){
						description = descrip;
						profileComp.put("Description", description);
					}else{
						if(descrip.contains(SECTOR)) {
							sector = descrip.replaceAll(SECTOR, "").trim();
							profileComp.put(SECTOR, sector);
						}
						if(descrip.contains(INDUSTRY)) {
							industry = descrip.replaceAll(INDUSTRY, "").trim();
							profileComp.put(INDUSTRY, industry);
						}
						if(descrip.contains(FISCALYEARENDS)) {
							fiscalYearEnds = descrip.replaceAll(FISCALYEARENDS, "").trim();
							profileComp.put(FISCALYEARENDS, fiscalYearEnds);
						}
						if(descrip.contains(EMPLOYEES)) {
							employees = descrip.replaceAll(EMPLOYEES, "").trim();
							profileComp.put(EMPLOYEES, employees);
						}
						if(descrip.contains(STOCKSTYLE)) {
							stocktyle = descrip.replaceAll(STOCKSTYLE, "").trim();
							profileComp.put(STOCKSTYLE, stocktyle);
						}
					}
				}
			}
		}
		
		return profileComp;
	}
	
	/**
	 * loadCompanyOutlookInfo
	*/
	public static Map<String, String> loadCompanyOutlookInfo(String nameComp, File file)
			throws IOException, NullPointerException {

		String event = "", nextEvent = "";

		Map<String, String> outLookComp = new HashMap<>();

		Document doc = Jsoup.parse(file, "UTF-8");
		Element element = doc.getElementById("OutlookHemscott");

		if(null != element) {
			Elements elements = element.children();
			
			for(Element elmt : elements) {
				String descrip = "";

				if(elmt.hasAttr("class") & elmt.attr("class").equalsIgnoreCase("item")) {
					descrip = elmt.text();
					int nbchild = elmt.children().size();

					if(nbchild == 0) {
						event = descrip;
						outLookComp.put("event", event);
					}else{
						if (elmt.children().size() == 1) {
							nextEvent = elmt.text();
						}
						outLookComp.put("nextEvent", nextEvent);
					}
				}
			}
		}
		
		return outLookComp;
	}
	
	/**
	 * loadCompanyFinancialInfo
	*/
	public static Map<String, String> loadCompanyFinancialInfo(String nameComp, File file)
			throws IOException, NullPointerException {

		Map<String, String> financialComp = new HashMap<>();
		List<String> years = new LinkedList<>();

		Document doc = Jsoup.parse(file, "UTF-8");
		Element element = doc.getElementById("OverviewFinancialsHemscott");

		if(null != element) {
			Element elemen = element.children().get(0);
			Elements elementTags = elemen.getElementsByTag("thead");
			Elements elementInsideThead = elementTags.get(0).children().get(0).children();

			// récupération des années correspondants aux rapports des finances
			for(Element elmtag : elementInsideThead) {
				if (elmtag.hasAttr("class") && elmtag.attr("class").equalsIgnoreCase("number")) {
					years.add(elmtag.text());
				}
			}

			// récupération des données avec les catégories correspondantes
			Elements elementsTbody = elemen.getElementsByTag("tbody");
			
			for(Element elmtbody : elementsTbody) {
				Elements tbodyChildren = elmtbody.children();
				Elements subheaders = elmtbody.getElementsByClass("subHeader");
				String category = "";

				if(null != subheaders && subheaders.size() == 1) {
					category = "_".concat(subheaders.get(0).text().trim().toUpperCase());
				}

				for(Element elt : tbodyChildren) {
					if(!elt.hasAttr("class")){
						if (elt.children().size() == 4) {
							String tex = elt.child(0).text().trim().toLowerCase();
							Element elm1 = elt.child(1);
							Element elm2 = elt.child(2);
							Element elm3 = elt.child(3);
							if (elm1.text().equalsIgnoreCase("_") || elm1.text().isEmpty()) {
								financialComp.put(tex.concat("_").concat(years.get(0)).concat("_").concat(category),
										"-");
							}else{
								financialComp.put(tex.concat("_").concat(years.get(0)).concat(category), elm1.text());
							}
							if(elm2.text().equalsIgnoreCase("_") || elm2.text().isEmpty()) {
								financialComp.put(tex.concat("_").concat(years.get(1)).concat(category), "-");
							}else{
								financialComp.put(tex.concat("_").concat(years.get(1)).concat(category), elm2.text());
							}
							if (elm3.text().equalsIgnoreCase("_") || elm3.text().isEmpty()) {
								financialComp.put(tex.concat("_").concat(years.get(2)).concat(category), "-");
							}else {

								financialComp.put(tex.concat("_").concat(years.get(2)).concat(category), elm3.text());
							}
						}

					}else if (elt.hasAttr("class") && (elt.attr("class").equalsIgnoreCase("alternate"))) {
						Element el = elt.getElementsByTag("th").first();
						String tex = "";
						
						if (el.children().size() == 0) {
							tex = el.text().trim().toLowerCase();
						}else{
							tex = el.child(0).text().trim().toLowerCase();
						}
						
						if(elt.children().size() == 4) {
							Element elm1 = elt.child(1);
							Element elm2 = elt.child(2);
							Element elm3 = elt.child(3);
							if (elm1.text().equalsIgnoreCase("_") || elm1.text().isEmpty()) {
								financialComp.put(tex.concat("_").concat(years.get(0)).concat(category), "-");
							}else{
								financialComp.put(tex.concat("_").concat(years.get(0)).concat(category), elm1.text());
							}
							if(elm2.text().equalsIgnoreCase("_") || elm2.text().isEmpty()) {
								financialComp.put(tex.concat("_").concat(years.get(1)).concat(category), "-");
							}else{
								financialComp.put(tex.concat("_").concat(years.get(1)).concat(category), elm2.text());
							}
							if(elm3.text().equalsIgnoreCase("_") || elm3.text().isEmpty()) {
								financialComp.put(tex.concat("_").concat(years.get(2)).concat(category), "-");
							}else{
								financialComp.put(tex.concat("_").concat(years.get(2)).concat(category), elm3.text());
							}
						}
					}
				}
			}
		}

		return financialComp;
	}
	
	/**
	 * companiesName
	*/
	public static List<String> companiesName() {
		List<String> compname = new ArrayList<>();
		File file = new File(Constants.COMPANIES_PROFILES_FILE_PATH);
		File[] files = file.listFiles();

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if(files[i].isDirectory() == false) {
					String name = files[i].getName().substring(0, files[i].getName().lastIndexOf("."));
					compname.add(name);
				}
			}
		}
		
		return compname;
	}
	
	/**
	 * companiesName
	*/
	public static String getCompanySymbol(String nameComp, File file) throws IOException, NullPointerException {
		Document doc = Jsoup.parse(file, "UTF-8");
		Elements element = doc.getElementsByTag("title");
		String symbol = "";

		if(null != element && element.size() == 1) {
			Element elt = element.get(0);
			String tex=elt.text().replaceAll("\\s","");
			String[] str = tex.split("\\|");
			
			if(str.length >= 2) {
				symbol = str[1];
			}
		}
 
		return symbol;
	}
	
	/**
	 * Charge la page investing.com afin de récuperer les dernières valeurs 
	 * correspondants à un Fx Spot 
	 * @return List<String> : Les valeurs résultantes  
	 */
	public static List<String> loadLastFxSpotValues() {
		List<String> values = new ArrayList<>();
		
		File file = new File(Constants.FXSPOT_HTML_FILE_PATH);
		try {
			Document document = Jsoup.parse(file, "UTF-8");
			
			if(null != document) {
				Element tableElement = document.getElementById(DATA_FXSPOT_TABLE_ID);
				Elements tBodies = tableElement.getElementsByTag("tbody");
				Elements tRs = tBodies.get(0).getElementsByTag("tr");
				
				//Première ligne du tableau en sautant la ligne
				//des titres des colonnes
				Element dataLigne = tRs.get(0);
				Elements columns = dataLigne.getElementsByTag("td");
				
				if(columns.size() == 6){
					//On contruit la liste 
					values.add(columns.get(0).ownText());
					values.add(columns.get(1).ownText());
					values.add(columns.get(2).ownText());
					values.add(columns.get(3).ownText());
					values.add(columns.get(4).ownText());
					
					//On recupère la valeur sans le symbole % Ex : 0.20% on veux 0.20
					String[] percentValue = columns.get(5).ownText().split("%");
					values.add(percentValue[0]);
				}
			}
		}catch (IOException exception) {
			System.out.println("JSOUP Impossible de parser le fichier");
			System.out.println(exception);
		}
		
		return values;
	}
	
	/**
     * transformStringToDouble
     * @param value La valeur à convertir
     * @return Le résultat 
    */
	public static Double transformStringToDouble(String value){
		Double nb=0.d;
		
		if( value.contains(",")){
			value=value.replaceAll(",", "");
			try{
			 nb=new Double(value);
			 
			}catch(NumberFormatException e){
				e.printStackTrace();
			}
		}
		return nb;
	}
}
