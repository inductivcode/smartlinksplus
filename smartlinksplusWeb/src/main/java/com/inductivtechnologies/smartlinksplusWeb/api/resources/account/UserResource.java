package com.inductivtechnologies.smartlinksplusWeb.api.resources.account;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.TokenLI;
import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.UserLI;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Credential;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.User;
import com.inductivtechnologies.smartlinksplusWeb.api.models.util.ApiResponse;

/**
 * RawMarketDataResourse : La ressource de type RawMarketData
*/
@Path("/user/")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class UserResource {
	
	/**
	 * Accès aux données de type User
	*/
	@EJB
	private UserLI userDao;
	
	/**
	 * Accès aux données de type Token
	*/
	@EJB
	private TokenLI tokenDao;
	
	/**
	 * Authentifie un utilisateur à partir de son nom utilisateur et son mot de passe 
	 * Ex url  /user/authenticate
	 * @param credential Les informations d'authentification 
	 * @return Le token de l'utilisateur correspondant ou null si aucun ne correspond
	*/
	@POST
	@Path("/auth")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response authenticateUser(Credential credential) {
		
		ApiResponse apiResponse = authenticate(credential);
		
		return Response.status(200).entity(apiResponse).build();
	}
	
	/**
	 * Authentifie un utilisateur à partir de son nom utilisateur et son mot de passe 
	 * @param credential Les informations d'authentification 
	 * @return ApiResponse
	*/
	private ApiResponse authenticate(Credential credential){

		//Réponse 
		ApiResponse apiResponse = new ApiResponse();
		
		int code = ApiResponse.CODE_SUCCESS;
		String message = "";
		Map<String, Object> data = new HashMap<String, Object>();
		
		User user = null;
		
		if(credential != null){
			user = userDao.findUserByCredentials(credential);
			if(user == null){
				code = ApiResponse.CODE_AUTHENTIFICATION_FAILED;
				message = "Utilisateur inconnu pour " + credential.getUsername();
			}else{
				code = ApiResponse.CODE_SUCCESS;
				message = "Utilisateur authentifié";
				data.put(Token.TOKEN_DATA_NAME, user.getToken());
			}
		}else{
			code = ApiResponse.CODE_INVALID_REQUEST;
			message = "Mauvaise requête : Informations manquantes";
		}
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		apiResponse.setData(data);
		
		return apiResponse;
	}
}
