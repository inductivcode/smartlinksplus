package com.inductivtechnologies.smartlinksplusWeb.api.resources.market;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.RawMarketDataLI;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusEjb.utils.Constants;
import com.inductivtechnologies.smartlinksplusEjb.utils.DateUtils;
import com.inductivtechnologies.smartlinksplusWeb.api.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusWeb.api.secure.SecureResource;

/**
 * RawMarketDataResourse : La ressource de type RawMarketData
*/
@Path("/data/market/")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class RawMarketDataResourse {
	
	/**
	 * Accès aux données de type RawMarketData
	*/
	@EJB
	private RawMarketDataLI utilRawMarketDataDAO;
			
	/**
	 * Retourne les equities entre deux dates 
	 * Ex url  data/market/equity/AAPL?startDate=01-01-2018&endDate=23-01-2018
	 * @param symbol Le symbole 
	 * @param startDate Date de début 
	 * @param endDate Date de fin 
	 * @return La liste des equities entre startDate et endDate au format JSON 
	*/
	@GET
	@SecureResource
	@Path("/equity/{symbol}")
	public Response equitiesBetweenTwoDate(@PathParam("symbol") final String symbol,
			@QueryParam("startDate") final String startDate,
			@QueryParam("endDate") final String endDate) {
		
		//Réponse 
		ApiResponse apiResponse = getMarketData(symbol, Constants.EQUITY_TYPE, startDate, endDate);
		
		return Response.status(200).entity(apiResponse).build();
	}
	
	/**
	 * Retourne les fx spot entre deux dates 
	 * Ex url  data/market/fxspot/EURUSD?startDate=01-01-2018&endDate=23-01-2018
	 * @param symbol Le symbole 
	 * @param startDate Date de début 
	 * @param endDate Date de fin 
	 * @return La liste des fx spot entre startDate et endDate au format JSON 
	*/
	@GET
	@SecureResource
	@Path("/fxspot/{symbol}")
	public Response fxspotBetweenTwoDate(@PathParam("symbol") final String symbol,
			@QueryParam("startDate") final String startDate,
			@QueryParam("endDate") final String endDate) {
		//Réponse 
		ApiResponse apiResponse = getMarketData(symbol, Constants.FXSPOT_TYPE, startDate, endDate);
		
		return Response.status(200).entity(apiResponse).build();
	}
	
	/**
	 * Retourne des marchés entre deux dates 
	 * @param symbol Le symbole 
	 * @param dataType Le type de marché
	 * @param startDate Date de début 
	 * @param endDate Date de fin 
	 * @return La liste des marchés entre startDate et endDate
	*/
	private ApiResponse getMarketData(final String symbol, final String dataType,
			final String startDate, 
			final String endDate){
		
		//Réponse 
		ApiResponse apiResponse = new ApiResponse();
		
		int code = ApiResponse.CODE_ERROR;
		String message = "";
		long dataCount = 0;
		Map<String, Object> data = new HashMap<String, Object>();
		
		if(!symbol.isEmpty() && !startDate.isEmpty() && !endDate.isEmpty()){
			//On vérifie les dates 
			if(DateUtils.isValidDate(startDate) && DateUtils.isValidDate(endDate)){
				Date eDate = DateUtils.parseJavaDate(endDate, Constants.PRINT_DATE_FORMAT);
				Date sDate = DateUtils.parseJavaDate(startDate, Constants.PRINT_DATE_FORMAT);
				 
				List<RawMarketData> marketsData = utilRawMarketDataDAO
						.rawMarketDataBetweenPeriod(symbol, dataType, sDate, eDate, 0, 0);
				
				if(marketsData.isEmpty()){
					//Pas de données
					code = ApiResponse.CODE_SUCCESS_NO_DATA;
					message = "Oops pas de données pour " + symbol;
				}else{
					code = ApiResponse.CODE_SUCCESS;
					message = "";
					dataCount = marketsData.size();
					data.put(dataType.toLowerCase(), marketsData);
				}
			}else{
				code = ApiResponse.CODE_INVALID_FORMAT_DATE;
				message = "Mauvaise requête : Date invalide";
			}
		}else{
			code = ApiResponse.CODE_ERROR;
			message = "Mauvaise requête";
		}
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		apiResponse.setDataCount(dataCount);
		apiResponse.setData(data);
		
		return apiResponse;
	}
	
}
