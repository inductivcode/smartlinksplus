package com.inductivtechnologies.smartlinksplusWeb.api.resources.account;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.TokenLI;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;
import com.inductivtechnologies.smartlinksplusWeb.api.models.util.ApiResponse;

/**
 * TokenRessource : La ressource de type Token
*/
@Path("/token/")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class TokenRessource {
	
	/**
	 * Accès aux données de type Token
	*/
	@EJB
	private TokenLI tokenDao;

	/**
	 * Vérifit l'existence et la validité d'un token 
	 * Ex url  /token/check
	 * @param value La valeur du token  
	*/
	@GET
	@Path("/check")
	public Response checkToken(@QueryParam("value") final String value){
		
		ApiResponse apiResponse = checkTokenValue(value);
		
		return Response.status(200).entity(apiResponse).build();
	}
	
	/**
	 * Vérifit l'existence et la validité d'un token 
	 * @param value La valeur du token  
	*/
	private ApiResponse checkTokenValue(final String value){
		//Réponse 
		ApiResponse apiResponse = new ApiResponse();
				
		int code = ApiResponse.CODE_SUCCESS;
		String message = "";
		Map<String, Object> data = new HashMap<String, Object>();
				
		if(value != null){
			Token token = tokenDao.findTokenByValue(value);
			if(token != null){
				if(!Util.tokenIsAlwaysValid(token)){
					Token newToken = tokenDao.regenerateToken(token);
					
					code = ApiResponse.CODE_TOKEN_EXPIRED;
					message = "Interdit : Token regénéré";
					data.put(Token.TOKEN_DATA_NAME, newToken);
				}else{
					code = ApiResponse.CODE_SUCCESS;
					message = "";
					data.put(Token.TOKEN_DATA_NAME, token);
				}
			}else{
				code = ApiResponse.CODE_TOKEN_UNEXIST;
				message = "Interdit : Accès restreint le token fournit n'existe pas";
			}
		}else{
			code = ApiResponse.CODE_INVALID_REQUEST;
			message = "Mauvaise requête : Informations manquantes";
		}
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		apiResponse.setData(data);
		
		return apiResponse;
	}
}
