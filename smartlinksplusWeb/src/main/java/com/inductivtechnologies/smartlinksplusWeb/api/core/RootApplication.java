package com.inductivtechnologies.smartlinksplusWeb.api.core;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * RootApplication : Servlet de base pour l'api
*/
@ApplicationPath("/api")
public class RootApplication extends Application{
}
