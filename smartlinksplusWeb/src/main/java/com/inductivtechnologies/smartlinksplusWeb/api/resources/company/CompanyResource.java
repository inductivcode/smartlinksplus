package com.inductivtechnologies.smartlinksplusWeb.api.resources.company;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.inductivtechnologies.smartlinksplusEjb.ejb.company.inter.CompanyLI;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.Company;
import com.inductivtechnologies.smartlinksplusWeb.api.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusWeb.api.secure.SecureResource;

/**
 * CompanyResource : La ressource Company
*/
@Path("/data/company")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class CompanyResource {

	/**
	 * Accès aux données de type Company
	*/
	@EJB
	private CompanyLI companyDao;

	/**
	 * Recherche une compagnie à partir de son symbole
	 * Ex url  data/company?symbol=AAPL
	 * @param symbol Le symbole 
	 * @return La compagnie correspondante
	*/
	@GET
	@SecureResource
	@Path("")
	public Response companyBySymbol(@QueryParam("symbol") final String symbol) {
		return Response.status(200).entity(getCompanyBySymbol(symbol)).build();
	}
	
	/**
	 * Retorune toutes les compagnies
	 * Ex url  data/company/all
	 * @return La liste de toutes compagnies 
	*/
	@GET
	@SecureResource
	@Path("/all")
	public Response allCompanies() {
		return Response.status(200).entity(getAllCompanies()).build();
	}
	
	/**
	 * Retorune toutes les compagnies
	 * @return La liste de toutes compagnies 
	*/
	private ApiResponse getAllCompanies(){
		//Réponse 
		ApiResponse apiResponse = new ApiResponse();
		int code = ApiResponse.CODE_ERROR;
		String message = "";
		Map<String, Object> data = new HashMap<String, Object>();
		
		List<Company> companies = companyDao.findAllCompanies();
		
		if(companies != null){
			apiResponse.setDataCount(companies.size());
			
			if(companies.isEmpty()){
				//Pas de données
				code = ApiResponse.CODE_SUCCESS_NO_DATA;
				message = "Oops pas de données. ";
			}else{
				code = ApiResponse.CODE_SUCCESS;
				data.put(Company.COMPANY_DATA_NAME_PLU, companies);
	
			}
		}
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		apiResponse.setData(data);
		
		return apiResponse;
	}
	
	/**
	 * Recherche une compagnie à partir de son symbole
	 * @param symbol Le symbole 
	 * @return La compagnie correspondante
	*/
	private ApiResponse getCompanyBySymbol(final String symbol){
		//Réponse 
		ApiResponse apiResponse = new ApiResponse();
		
		int code = ApiResponse.CODE_ERROR;
		String message = "";
		Map<String, Object> data = new HashMap<String, Object>();
		
		if(!symbol.isEmpty()){
			Company company = companyDao.findCompanyBySymbol(symbol);
			
			if(company == null){
				code = ApiResponse.CODE_SUCCESS_NO_DATA;
				message = "Aucune compagnie pour le symbole << " + symbol + " >>";
			}else{
				code = ApiResponse.CODE_SUCCESS;
				message = "";
				apiResponse.setDataCount(1);
				data.put(Company.COMPANY_DATA_NAME, company);
			}
		}else{
			code = ApiResponse.CODE_ERROR;
			message = "Mauvaise requête";
		}
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		apiResponse.setData(data);
		
		return apiResponse;
	}
	
}
