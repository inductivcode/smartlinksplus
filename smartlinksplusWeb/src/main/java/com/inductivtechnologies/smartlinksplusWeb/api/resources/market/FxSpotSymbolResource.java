package com.inductivtechnologies.smartlinksplusWeb.api.resources.market;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.inductivtechnologies.smartlinksplusEjb.ejb.market.inter.FxSpotSymbolLI;
import com.inductivtechnologies.smartlinksplusEjb.entities.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusWeb.api.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusWeb.api.secure.SecureResource;

/**
 * FxSpotSymbolResource : La ressource de type FxSpotSymbol
*/
@Path("/data/fxspotsymbol")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class FxSpotSymbolResource {

	/**
	 * Accès aux données de type FxSpotSymbol
	*/
	@EJB
	private FxSpotSymbolLI fxSpotSymbolDao;

	/**
	 * Retourne tous les symbols fx spot
	 * Ex url  /data/fxspotsymbol/all
	 * @return La liste des symboles
	*/
	@GET
	@SecureResource
	@Path("/all")
	public Response allSymbols(){
		return Response.status(200).entity(getAllSymbols()).build();
	}
	
	/**
	 * Recherche et retourne un fxspotsymbol en se basant sur sa valeur
	 * Ex url  /data/fxspotsymbol?symbol=EURUSD
	 * @return Le symbole trouvé
	*/
	@GET
	@SecureResource
	@Path("")
	public Response fxSpotSymbolByValue(@QueryParam("symbol") final String symbol){
		return Response.status(200).entity(getFxSpotSymbolByValue(symbol)).build();
	}
	
	/**
	 * Recherche et retourne un fxspotsymbol en se basant sur sa valeur
	 * @return Le symbole trouvé
	*/
	private ApiResponse getFxSpotSymbolByValue(final String symbol){
		//Réponse 
		ApiResponse apiResponse = new ApiResponse();
		
		int code = ApiResponse.CODE_ERROR;
		String message = "";
		Map<String, Object> data = new HashMap<String, Object>();
		
		if(!symbol.isEmpty()){
			FxSpotSymbol fxSpotS = fxSpotSymbolDao.findFxSpotSymbol(symbol);
			
			if(fxSpotS == null){
				code = ApiResponse.CODE_SUCCESS_NO_DATA;
				message = "Aucun FXSPOTSYMBOL pour le symbole << " + symbol + " >>";
			}else{
				code = ApiResponse.CODE_SUCCESS;
				message = "";
				apiResponse.setDataCount(1);
				data.put(FxSpotSymbol.FXSPOTSYMBOL_DATA_NAME_SIN, fxSpotS);
			}
		}else{
			code = ApiResponse.CODE_ERROR;
			message = "Mauvaise requête";
		}
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		apiResponse.setData(data);
		
		return apiResponse;
	}
	
	/**
	 * Retourne tous les symbols fx spot
	 * @return La liste des symboles
	*/
	private ApiResponse getAllSymbols(){
		//Réponse 
		ApiResponse apiResponse = new ApiResponse();
		
		int code = ApiResponse.CODE_ERROR;
		String message = "";
		Map<String, Object> data = new HashMap<String, Object>();
		
		List<FxSpotSymbol> symbols = fxSpotSymbolDao.allFxSpotSymbol();
		
		if(symbols != null){
			apiResponse.setDataCount(symbols.size());
			
			if(symbols.isEmpty()){
				//Pas de données
				code = ApiResponse.CODE_SUCCESS_NO_DATA;
				message = "Oops pas de données.";
			}else{
				code = ApiResponse.CODE_SUCCESS;
				data.put(FxSpotSymbol.FXSPOTSYMBOL_DATA_NAME, symbols);
			}
		}
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		apiResponse.setData(data);
		
		return apiResponse;
	}
	
}
