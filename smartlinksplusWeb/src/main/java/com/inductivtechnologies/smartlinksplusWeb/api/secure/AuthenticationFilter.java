package com.inductivtechnologies.smartlinksplusWeb.api.secure;

import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.ws.rs.ext.Provider;

import com.inductivtechnologies.smartlinksplusEjb.ejb.account.inter.TokenLI;
import com.inductivtechnologies.smartlinksplusEjb.entities.account.Token;
import com.inductivtechnologies.smartlinksplusEjb.utils.Util;
import com.inductivtechnologies.smartlinksplusWeb.api.models.util.ApiResponse;

import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;

/**
 * Intercepte toutes les réquêtes qui permettent
 * d'acceder aux ressources sécurisées
*/
@SecureResource
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter{
	
	private static final String TOKEN_SCHEME = "auth-token";
	
	/**
	 * Accès aux données de type Token
	*/
	@EJB
	private TokenLI tokenDao;

	@Override
	public void filter(ContainerRequestContext requestContext) {
        // On extrait le token de l'en-tête  
        String token = requestContext.getHeaderString(TOKEN_SCHEME);
        
        if(token == null || token.isEmpty()){
        	  abortWithUnauthorized(requestContext,
              		"Interdit : Utilisateur non authentifié",
              		ApiResponse.CODE_AUTHENTIFICATION_FAILED);
        	return;
        }
        	
        // On valide le token 
        validateToken(requestContext, token);
	}

	/**
	 * Abandonne l'action de filtrage
	 * @param authorizationHeader Représentation de l'en-tete authorisation
	 * @return Vrai si l'en-ttête est correcte Faux sinon 
	*/
	private void abortWithUnauthorized(ContainerRequestContext requestContext, String message , int code) {
		ApiResponse apiResponse = new ApiResponse();
		
		apiResponse.setMessage(message);
		apiResponse.setCode(code);
		
		//On annule la requête avec une réponse personnalisée
	    requestContext.abortWith(
	        Response.status(200).entity(apiResponse).build());
	}

	/**
	 * Valide le token 
	 * @param token Le token à valider 
	*/
	private void validateToken(ContainerRequestContext requestContext, String token) {
		Token tokenResult = tokenDao.findTokenByValue(token);
		
		if(tokenResult == null){
			//Le token n'existe pas 
			abortWithUnauthorized(requestContext,
              		"Interdit : Accès interdit clé d'accès non valide",
              		ApiResponse.CODE_AUTHENTIFICATION_FAILED);
		}else{
			if(!Util.tokenIsAlwaysValid(tokenResult)){
				//Le token a expiré 
				abortWithUnauthorized(requestContext,
	              		"Interdit : Token expiré",
	              		ApiResponse.CODE_TOKEN_EXPIRED);
			}
		}
	}
}
